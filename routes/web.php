<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect('/inicio');
});

Route::get('/home', 'PageController@home');
Route::get('/group', 'PageController@group');
Route::get('/group/innovation', 'PageController@grupo_innovacion_en');
Route::get('/group/sustainable-development', 'PageController@grupo_desarrollo_sostenible_en');
Route::get('/group/social-responsability', 'PageController@grupo_responsabilidad_social_en');



/*
Route::get('/contact', 'PageController@contactanos');
Route::get('/group', 'PageController@grupo');
Route::get('/group/internal', 'PageController@grupoShow');
Route::get('/business', 'PageController@empresas');
Route::get('/sustainability', 'PageController@sostenibilidad');
Route::get('/sustainability/internal', 'PageController@sostenibilidadShow');
Route::get('/news', 'PageController@novedades');
Route::get('/business/foods', 'PageController@alimentos');
Route::get('/business/solutions', 'PageController@soluciones');
Route::get('/business/agroindustry', 'PageController@agroindustria');
Route::get('/business/papers', 'PageController@papeles');
Route::get('/business/others', 'PageController@otros');
Route::get('/news/internal', 'PageController@novedadesShow');
Route::get('/business/internal', 'PageController@empresasShow');*/


Route::get('/inicio', 'PageController@index');

Route::get('/contactanos', 'PageController@contactanos');


Route::get('/grupo', 'PageController@grupo');
Route::get('/grupo/innovacion', 'PageController@grupo_innovacion_es');
Route::get('/grupo/desarrollo-sostenible', 'PageController@grupo_desarrollo_sostenible_es');
Route::get('/grupo/responsabilidad-social', 'PageController@grupo_responsabilidad_social_es');



Route::get('/grupo/interna', 'PageController@grupoShow');
Route::get('/empresas', 'PageController@empresas');
Route::get('/sostenibilidad', 'PageController@sostenibilidad');
Route::get('/sostenibilidad/interna', 'PageController@sostenibilidadShow');
Route::get('/novedades', 'PageController@novedades');
Route::get('/empresas/alimentos', 'PageController@alimentos');
Route::get('/empresas/soluciones', 'PageController@soluciones');
Route::get('/empresas/agroindustria', 'PageController@agroindustria');
Route::get('/empresas/papeles', 'PageController@papeles');
Route::get('/empresas/otros', 'PageController@otros');
Route::get('/novedades/interna', 'PageController@novedadesShow');
Route::get('/empresa/interna', 'PageController@empresasShow');
<<<<<<< HEAD

Route::get('/lang/{lang}', 'Multilingual@idioma')->where(['lang' => 'en|es']);

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
=======
Route::get('/pdf', 'PageController@pdf');
>>>>>>> c2e96db4a38758992f11496e85ff8fbcb980dd0a
