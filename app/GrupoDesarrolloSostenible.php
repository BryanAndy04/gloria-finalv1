<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoDesarrolloSostenible extends Model
{
    protected $table = "grupo_desarrollo_sostenible";
}
