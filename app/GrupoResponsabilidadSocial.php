<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoResponsabilidadSocial extends Model
{
    protected $table = "grupo_responsabilidad_social";
}
