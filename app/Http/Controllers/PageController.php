<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use App\InicioBanner;
use App\iniciovideo;
use App\GrupoBanner;
use App\QuienesSomos;
use App\GrupoHistoria;
use App\GrupoDesarrolloSostenible;
use App\GrupoResponsabilidadSocial;
use App\GrupoInnovacion;

class PageController extends Controller
{
    public function index()
    {

        App::setLocale('es');

        $banner = InicioBanner::orderBy('orden','asc')->get();
        $video  = InicioVideo::first();
        return view('frontend.home.index', [
                                              'idioma' => 'es',
                                              'banner' => $banner,
                                              'video'  => $video
                                           ]);
    }


    public function home()
    {
        App::setLocale('en');

        $banner = InicioBanner::orderBy('orden','asc')->get();
        $video  = InicioVideo::first();
        return view('frontend.home.index', [
                                              'idioma' => 'en',
                                              'banner' => $banner,
                                              'video'  => $video
                                           ]);
    }



    public function group()
    {
        App::setLocale('en');
        $banner = GrupoBanner::first();
        $quienes_somos = QuienesSomos::first();
        $historia = GrupoHistoria::orderBy('orden','asc')->get();
        $innovacion = GrupoInnovacion::first();
        $grupo_desarrollo_sostenible = GrupoDesarrolloSostenible::first();
        $grupo_responsabilidad_social = GrupoResponsabilidadSocial::first();
        return view('frontend.grupo.index', [ 'idioma' => 'en', 'banner' => $banner, 'quienes_somos' => $quienes_somos, 'historia' => $historia, 'innovacion' => $innovacion, 'grupo_desarrollo_sostenible' => $grupo_desarrollo_sostenible, 'grupo_responsabilidad_social' => $grupo_responsabilidad_social ]);
    }

    public function grupo()
    {
        App::setLocale('es');
        $banner = GrupoBanner::first();
        $quienes_somos = QuienesSomos::first();
        $historia = GrupoHistoria::orderBy('orden','asc')->get();
        $innovacion = GrupoInnovacion::first();
        $grupo_desarrollo_sostenible = GrupoDesarrolloSostenible::first();
        $grupo_responsabilidad_social = GrupoResponsabilidadSocial::first();
        return view('frontend.grupo.index', [ 'idioma' => 'es', 'banner' => $banner, 'quienes_somos' => $quienes_somos, 'historia' => $historia, 'innovacion' => $innovacion, 'grupo_desarrollo_sostenible' => $grupo_desarrollo_sostenible, 'grupo_responsabilidad_social' => $grupo_responsabilidad_social]);
    }

    public function grupo_innovacion_es()
    {
        App::setLocale('es');
        $innovacion = GrupoInnovacion::first();
        return view('frontend.grupo.innovacion', [ 'idioma' => 'es', 'innovacion' => $innovacion]);
    }


    public function grupo_innovacion_en()
    {
        App::setLocale('en');
        $innovacion = GrupoInnovacion::first();
        return view('frontend.grupo.innovacion', [ 'idioma' => 'en', 'innovacion' => $innovacion]);
    }

    public function grupo_desarrollo_sostenible_es()
    {
        App::setLocale('es');
        $grupo_desarrollo_sostenible = GrupoDesarrolloSostenible::first();
        return view('frontend.grupo.desarrollo_sostenible', [ 'idioma' => 'es', 'grupo_desarrollo_sostenible' => $grupo_desarrollo_sostenible]);
    }


    public function grupo_desarrollo_sostenible_en()
    {
        App::setLocale('en');
        $grupo_desarrollo_sostenible = GrupoDesarrolloSostenible::first();
        return view('frontend.grupo.desarrollo_sostenible', [ 'idioma' => 'en', 'grupo_desarrollo_sostenible' => $grupo_desarrollo_sostenible]);
    }


    public function grupo_responsabilidad_social_es()
    {
        App::setLocale('es');
        $grupo_responsabilidad_social = GrupoResponsabilidadSocial::first();
        return view('frontend.grupo.responsabilidadsocial', [ 'idioma' => 'es', 'grupo_responsabilidad_social' => $grupo_responsabilidad_social]);
    }


    public function grupo_responsabilidad_social_en()
    {
        App::setLocale('en');
        $grupo_responsabilidad_social = GrupoResponsabilidadSocial::first();
        return view('frontend.grupo.responsabilidadsocial', [ 'idioma' => 'en', 'grupo_responsabilidad_social' => $grupo_responsabilidad_social]);
    }

    public function contactanos()
    {
        return view('frontend.contactanos.index');
    }



    public function alimentos()
    {
        return view('frontend.alimentos.index');
    }
    public function soluciones()
    {
        return view('frontend.soluciones.index');
    }

    public function agroindustria()
    {
        return view('frontend.agroindustria.index');
    }

    public function papeles()
    {
        return view('frontend.papeles.index');
    }
    public function pdf()
    {
        return view('frontend.partials.pdf');
    }

    public function otros()
    {
        return view('frontend.otros.index');
    }

    public function sostenibilidad()
    {
        return view('frontend.sostenibilidad.index');
    }

    public function sostenibilidadShow()
    {
        return view('frontend.sostenibilidad.show');
    }

    public function novedades()
    {
        return view('frontend.novedades.index');
    }

    public function novedadesShow()
    {
        return view('frontend.novedades.show');
    }

    public function empresas()
    {
        return view('frontend.empresa.index');
    }
    public function empresasShow()
    {
        return view('frontend.empresa.show');
    }
}
