@extends('frontend.layout.layout')
@section('content')
@include('frontend.partials.menu')

<section class="empresa" style="background-image:url({{ url('images/conoce/centroynitrato.jpg') }})">
  <div class="empresa__titulo">
      <div>
        <img src="{{ url('images/iconos/construccion-g.png') }}" alt="">
          <h1 class="titulo titulo--grande">Soluciones para <br> la <span>construcción <br> y minería</span> </h1>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. </p>
          <div class="botones">
            <a href="javascript:void(0)" class="botton botton--blanco bannerTop">Introducción</a>
            <a href="javascript:void(0)" class="botton botton--completo bannerEmpresa">Ver empresas</a>
          </div>
      </div>
  </div>
  <div class="container-fluid ocultoResponsive">
    <div class="row empresa__bottom">
      <div class="col p-0 ">
        <a href="{{ url('/empresas/alimentos') }}" class="empresa__bottom__item">
          <img src="{{ url('images/iconos/alimentos.png') }}" alt="">
          <h4>Alimentos</h4>
        </a>
      </div>
      <div class="col p-0 active">
        <a href="{{ url('/empresas/soluciones') }}" class="empresa__bottom__item">
          <img src="{{ url('images/iconos/construccion.png') }}" alt="">
          <h4>Soluciones para la construcción y minería</h4>
        </a>
      </div>
      <div class="col p-0">
        <a href="{{ url('/empresas/agroindustria') }}" class="empresa__bottom__item">
          <img src="{{ url('images/iconos/agroindustria.png') }}" alt="">
          <h4>agroindustria</h4>
        </a>
      </div>
      <div class="col p-0">
        <a href="{{ url('/empresas/papeles') }}" class="empresa__bottom__item">
          <img src="{{ url('images/iconos/papeles.png') }}" alt="">
          <h4>Papeles y Cartones</h4>
        </a>
      </div>
      <div class="col p-0">
        <a href="{{ url('/empresas/otros') }}" class="empresa__bottom__item">
          <img src="{{ url('images/iconos/otros.png') }}" alt="">
          <h4>otros negocios</h4>
        </a>
      </div>
    </div>
  </div>
</section>
<section class="secciones p-0 intro">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 p-0">
                <div class="secciones__img secciones--padding">
                <img src="{{ url('images/conoce/soluciones/intro.png') }}" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-lg-5 d-flex align-items-center">
                <div class="secciones__content">
                    <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>Introducción </span></h2>
                    <div class="">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</p>
                        <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. Phasellus pede arcu, dapibus eu, fermentum et, dapibus sed, urna.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="empresa__content empresaBotom">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 offset-xl-1 col-lg-5 col-md-6">
        <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>empresas</span> </h2>
      </div>
      <div class="col-lg-7 col-xl-4 col-md-6">
        <div class="empresa__content__filtro">
          <form class="row d-flex align-items-center" action="index.html" method="post">
            <div class="col-lg-3">
              <label for="">Seleccionar país:</label>
            </div>
            <div class="col-lg-4">
              <select id="pais">
                  <option value="hide"> Todos</option>
                  <option value="2010">2010</option>
                  <option value="2011">2011</option>
                  <option value="2012">2012</option>
                  <option value="2013">2013</option>
                  <option value="2014">2014</option>
                  <option value="2015">2015</option>
              </select>
            </div>
            <div class="col-lg-5 p-0">
              <div class="buscador">
                <input type="text" name="" placeholder="Buscar Empresa" value="">
                <button type="button" name="button" class="buscar"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-xl-3">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url({{ url('images/alimentos/empresa/1.jpg') }})">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Perú</span>
              <h3>Gloria S.A.</h3>
              <p>Hace 46 años se constituyó Yura S.A., para ser uno de los ejes de desarrollo más importantes de la región sur del país.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
              <a href="#">visitar web</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-xl-3">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url( {{ url('images/alimentos/empresa/2.jpg') }} )">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Perú</span>
              <h3>Deprodeca S.A.C</h3>
              <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-xl-3">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url( {{ url('images/alimentos/empresa/3.jpg') }} )">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Puerto rico</span>
              <h3>Suiza Dairy CO.</h3>
              <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
              <a href="#">visitar web</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-xl-3">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url( {{ url('images/alimentos/empresa/4.jpg') }} )">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Puerto rico</span>
              <h3>Suiza Fruit CO.</h3>
              <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
              <a href="#">visitar web</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-xl-3">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url( {{ url('images/alimentos/empresa/5.jpg') }} )">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Bolivia</span>
              <h3>Pil Andina S.A.</h3>
              <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
              <a href="#">visitar web</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-xl-3">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url( {{ url('images/alimentos/empresa/6.jpg') }} )">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Argentina</span>
              <h3>Corlasa S.A.</h3>
              <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
              <a href="#">visitar web</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-xl-3">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url( {{ url('images/alimentos/empresa/7.jpg') }} )">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Colombia</span>
              <h3>Algarra S.A.</h3>
              <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
              <a href="#">visitar web</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 col-xl-3">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url( {{ url('images/alimentos/empresa/8.jpg') }} )">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Ecuador</span>
              <h3>Leansa S.A.</h3>
              <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
              <a href="#">visitar web</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@include('frontend.partials.footer')
@include('frontend.partials.modal')
@endsection

@section('scripts')

@stop
