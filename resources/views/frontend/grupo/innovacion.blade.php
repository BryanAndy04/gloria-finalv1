@extends('frontend.layout.layout')
@section('content')
@include('frontend.partials.menu')

<span id='pestana_vista' valor='grupo'></span>

<section class="novedades__interna">
  <section class="novedades__interna__top">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 text-center m-auto">
          <div class="item">
            <a href="{{ url('/') }}/{{ trans('gloria.r_grupo') }}" class="botton botton--normal">{{ trans('gloria.regresa') }}</a>
            <h2>{{ trans('gloria.inovacion') }}</h2>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="novedades__interna__bottom">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 m-auto p-0">
          <div class="parrafoCentrado">
            <img src="{{ url('images/iconos/raya-cuadrado.png') }}" class="cuadrado" alt="">
            <p><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt="">

                @if ( $idioma == 'es' )
                    {{ $innovacion->descripcion_principal }}
                @else
                    {{$innovacion->descripcion_principal_en }}
                @endif

            </p>
          </div>
        </div>
        <div class="col-lg-10 m-auto">
          <div class="owl-carousel owl-interna">
            <div class="item">
              <img src="{{ url('storage') }}/{{ $innovacion->imagen }}" alt="" class="img-fluid">
            </div>
          </div>
        </div>
        <div class="col-lg-6 m-auto p-0">
          <div class="contenido">
            @if ( $idioma == 'es' )
               {!! html_entity_decode($innovacion->descripcion) !!}
            @else
               {!! html_entity_decode($innovacion->descripcion_en) !!}
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>
</section>



@include('frontend.partials.footer')
@include('frontend.partials.modal')
@endsection

@section('scripts')

@stop
