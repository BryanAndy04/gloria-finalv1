@extends('frontend.layout.layout')
@section('content')
@include('frontend.partials.menu')
<span id='pestana_vista' valor='grupo'></span>

<section class="banner" style="background-image:url('{{ url('storage') }}/{{ $banner->imagen }}')">
    <div class="banner__titulo">
        <div>
            <h1><span>@if($idioma == 'es'){{ $banner->titulo }}@else{{ $banner->titulo_en }}@endif </span>  @if($idioma == 'es'){{ $banner->titulo_add }}@else{{ $banner->titulo_add_en }}@endif</h1>
            <p>@if($idioma == 'es'){{ $banner->descripcion }}@else{{ $banner->descripcion_en }}@endif</p>
        </div>
    </div>
    <a class="scroll-down hash-link" href="javascript:void(0)" title="Gloria">
      <i>
        <svg class="arrow" width="7" height="5" viewBox="0 0 7 5" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><path id="arrow" fill="#FFF" fill-rule="nonzero" d="M0 0l3.5 5L7 0z"></path></g></svg>
        <svg class="circle" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
          <g class="circle-wrap" fill="none" stroke="#fff" stroke-width="1" stroke-linejoin="round" stroke-miterlimit="10">
              <circle cx="12" cy="12" r="10.5"></circle>
          </g>
        </svg>
      </i>
    </a>
</section>
<section class="secciones seccionPage" style="padding-top:0;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 p-0">
                <div class="secciones__img secciones--padding">
                <img src="{{ url('storage') }}/{{ $quienes_somos->imagen }}" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-lg-5 d-flex align-items-center">
                <div class="secciones__content">
                    <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>{{ trans('gloria.quienes') }} </span>{{ trans('gloria.somos') }}</h2>
                    <div class="">
                        @if ( $idioma == 'es' )
                           {!! html_entity_decode($quienes_somos->descripcion) !!}
                        @else
                           {!! html_entity_decode($quienes_somos->descripcion_en) !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="historia">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="historia-content">

          @foreach ($historia as $historia__)

          <div>
            <div class="row">
              <div class="col-xl-5 offset-xl-1 d-flex align-items-center">
                <div class="historia-content__texto">
                  <h3 class="anio__grande">{{ $historia__->ano }}</h3>
                  <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""> <span>{{ trans('gloria.historia') }}</span> </h2>
                  <div class="item">
                    <span class="anio__chico">{{ $historia__->ano }}</span>
                    <div class="item__texto">
                        @if ( $idioma == 'es' )
                            {!! html_entity_decode($historia__->descripcion) !!}
                        @else
                            {!! html_entity_decode($historia__->descripcion_en) !!}
                        @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-5">
                <div class="historia-content__img">
                  <img src="{{ url('storage') }}/{{ $historia__->imagen }}" class="img-fluid" alt="">
                </div>
              </div>
            </div>
          </div>

          @endforeach

        </div>

        <div class="historia-nav">
          @foreach ($historia as $historia__)
          <div class="historia-nav__item">
            {{ $historia__->ano }}
            <div class="circulo">
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
<section class="secciones" style="padding-top:0;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 p-0">
                <div class="secciones__img">
                <img src="{{ url('storage') }}/{{ $innovacion->imagen }}" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-lg-5 d-flex align-items-center">
                <div class="secciones__content">
                    <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>{{ trans('gloria.inovacion') }} </span></h2>
                    <div class="">
                        @if ( $idioma == 'es' )
                            {!! html_entity_decode($innovacion->descripcion) !!}
                        @else
                            {!! html_entity_decode($innovacion->descripcion_en) !!}
                        @endif
                    </div>
                    <a href="{{ url('/') }}/{{ trans('gloria.r_grupo_innovacion') }}" class="botton botton--normal">{{ trans('gloria.vermas') }}</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7 p-0">
                <div class="secciones__img">
                <img src="{{ url('storage') }}/{{ $grupo_desarrollo_sostenible->imagen }}" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-lg-5 d-flex align-items-center">
                <div class="secciones__content">
                    <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>{{ trans('gloria.desarrollo') }} </span>{{ trans('gloria.sostenible') }}</h2>
                    <div class="">
                        @if ( $idioma == 'es' )
                        {!! html_entity_decode($grupo_desarrollo_sostenible->descripcion) !!}
                        @else
                            {!! html_entity_decode($grupo_desarrollo_sostenible->descripcion_en) !!}
                        @endif
                    </div>
                    <a href="{{ url('/') }}/{{ trans('gloria.r_grupo_desarrollo_sostenible') }}" class="botton botton--normal">{{ trans('gloria.vermas') }}</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7 p-0">
                <div class="secciones__img">
                <img src="{{ url('storage') }}/{{ $grupo_responsabilidad_social->imagen }}" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-lg-5 d-flex align-items-center">
                <div class="secciones__content">
                    <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>{{ trans('gloria.responsabilidad') }}  </span>{{ trans('gloria.social') }}</h2>
                    <div class="">
                        @if ( $idioma == 'es' )
                        {!! html_entity_decode($grupo_responsabilidad_social->descripcion) !!}
                        @else
                            {!! html_entity_decode($grupo_responsabilidad_social->descripcion_en) !!}
                        @endif
                    </div>
                    <a href="{{ url('/') }}/{{ trans('gloria.r_grupo_responsabilidad_social') }}" class="botton botton--normal">{{ trans('gloria.vermas') }}</a>
                </div>
            </div>
        </div>
    </div>
</section>


@include('frontend.partials.footer')
@include('frontend.partials.modal')
@endsection

@section('scripts')

@stop
