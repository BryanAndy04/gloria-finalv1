@extends('frontend.layout.layout')
@section('content')
@include('frontend.partials.menu')
<span id='pestana_vista' valor='contactanos'></span>
<section class="banner" style="background-image:url( {{ url('images/banner/banner-contactanos.jpg') }} )">
    <div class="banner__titulo">
        <div>
            <h1>Contáctenos</h1>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. </p>
        </div>
    </div>
    <a class="scroll-down hash-link" href="javascript:void(0)" title="Gloria">
  		<i>
  			<svg class="arrow" width="7" height="5" viewBox="0 0 7 5" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><path id="arrow" fill="#FFF" fill-rule="nonzero" d="M0 0l3.5 5L7 0z"></path></g></svg>
  			<svg class="circle" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
          <g class="circle-wrap" fill="none" stroke="#fff" stroke-width="1" stroke-linejoin="round" stroke-miterlimit="10">
              <circle cx="12" cy="12" r="10.5"></circle>
          </g>
        </svg>
  		</i>
  	</a>
</section>
<section class="contacto seccionPage">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-xl-3">
                <div class="contacto__info">
                    <div class="item">
                        <img src="{{ url('images/iconos/location.png') }}" alt="">
                        <h4>Dirección</h4>
                        <p>Av. República de Panamá 2461 – Santa <br> Catalina, La Victoria <br> Lima 13 - PERÚ</p>
                    </div>
                    <div class="item">
                        <img src="{{ url('images/iconos/phone.png') }}" alt="">
                        <h4>Teléfono</h4>
                        <p><a href="(0051 1) 470 7170">(0051 1) 470 7170</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-xl-9">
                <div class="contacto__content row">
                    <div class="col-lg-12">
                        <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>Escríbenos un mensaje</span> </h2>
                    </div>
                    <div class="col-xl-9 col-lg-11">
                        <div class="form">
                            <form class="row">
                                <div class="form-group col-lg-6">
                                    <input type="text" class="form-control" placeholder="Enter email">
                                    <label >Nombre completo</label>
                                </div>
                                <div class="form-group col-lg-6">
                                    <input type="mail" class="form-control" placeholder="empresa@dominio.com">
                                    <label >Correo</label>
                                </div>
                                <div class="form-group col-lg-6">
                                    <input type="text" class="form-control" placeholder="123456789">
                                    <label >Teléfono</label>
                                </div>
                                <div class="form-group col-lg-6">
                                    <input type="text" class="form-control" placeholder="Asunto">
                                    <label>Ingresar asunto</label>
                                </div>
                                <div class="form-group col-lg-12">
                                  <label for="exampleInputEmail1" class="label">Tipo de consulta</label>
                                    <select id="consulta">
                                        <option value="hide">Seleccionar tipo de consulta</option>
                                        <option value="2010">2010</option>
                                        <option value="2011">2011</option>
                                        <option value="2012">2012</option>
                                        <option value="2013">2013</option>
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-12">
                                    <textarea name="" class="form-control" placeholder="Saludos..." cols="30" rows="10"></textarea>
                                    <label for="exampleInputPassword1">Mensaje</label>
                                </div>
                                <div class="col-lg-12 form__boton">
                                  <span>Al enviar acepta los términos y condiciones</span>
                                  <button type="submit" class="botton botton--normal">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="contenidoFlecha">
  <section class="mapa">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3901.3601976283885!2d-77.02621768457503!3d-12.087475445951679!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c862e4fe0597%3A0x9c41f41b85a727bd!2sGrupo%20Gloria!5e0!3m2!1ses-419!2spe!4v1592807963003!5m2!1ses-419!2spe" width="800" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
      <div class="mapa__content">
          <div class="mapa__content__top">
              <h3>Empresas del grupo</h3>
              <label for="">Seleccionar país:</label>
              <select id="pais">
                  <option value="hide"> Perú</option>
                  <option value="bolivia"> Bolivia</option>
                  <option value="chile"> Chile</option>
              </select>
          </div>
          <div class="mapa__content__bottom container">
              <a href="javascript:void(0)" class="item row listados">
                  <div class="col-lg-8 col-9">
                    <div class="item__content">
                      <h4>Gloria S.A</h4>
                      <p>Av. República de Panamá 2461, Santa Catalina, Lima 13, Perú</p>
                      <p>(0051 1) 470 7170</p>
                      <p>contactenos@gloria.com.pe</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-3 d-flex justify-content-center align-items-center">
                    <img src="{{ url('images/iconos/gloria.jpg') }}"  class="img-fluid" alt="">
                  </div>
              </a>
              <a href="javascript:void(0)" class="item row listados">
                  <div class="col-lg-8 col-9">
                    <div class="item__content">
                      <h4>DEPRODECA</h4>
                      <p>Av. República de Panamá 2457, Santa Catalina, Lima 13, Perú</p>
                      <p>(0051 1) 470 7170</p>
                      <p>contactenos@deprodeca.com.pe</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-3 d-flex justify-content-center align-items-center">
                    <img src="{{ url('images/iconos/gloria.jpg') }}"  class="img-fluid" alt="">
                  </div>
              </a>
              <a href="javascript:void(0)" class="item row listados">
                  <div class="col-lg-8 col-9">
                    <div class="item__content">
                      <h4>YURA S.A.</h4>
                      <p>Estación Yura, . s/n, Distrito de Yura Arequipa - Perú</p>
                      <p>(0051 54) 49-5060</p>
                      <p>contactos_yura@gloria.com.pe</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-3 d-flex justify-content-center align-items-center">
                    <img src="{{ url('images/iconos/gloria.jpg') }}"  class="img-fluid" alt="">
                  </div>
              </a>
              <a href="javascript:void(0)" class="item row listados">
                  <div class="col-lg-8 col-9">
                    <div class="item__content">
                      <h4>Cal & Cemento Sur S.A.</h4>
                      <p>Carretera al Sur Km 11, Caracoto, Juliaca Puno - Perú</p>
                      <p>(0051 51) 32-8544</p>
                      <p>contactenos_cesur@gloria.com.pe</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-3 d-flex justify-content-center align-items-center">
                    <img src="{{ url('images/iconos/gloria.jpg') }}"  class="img-fluid" alt="">
                  </div>
              </a>
              <a href="javascript:void(0)" class="item row listados">
                  <div class="col-lg-8 col-9">
                    <div class="item__content">
                      <h4>YURA S.A.</h4>
                      <p>Estación Yura, . s/n, Distrito de Yura Arequipa - Perú</p>
                      <p>(0051 54) 49-5060</p>
                      <p>contactos_yura@gloria.com.pe</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-3 d-flex justify-content-center align-items-center">
                    <img src="{{ url('images/iconos/gloria.jpg') }}"  class="img-fluid" alt="">
                  </div>
              </a>
              <a href="javascript:void(0)" class="item row listados">
                  <div class="col-lg-8 col-9">
                    <div class="item__content">
                      <h4>YURA S.A.</h4>
                      <p>Estación Yura, . s/n, Distrito de Yura Arequipa - Perú</p>
                      <p>(0051 54) 49-5060</p>
                      <p>contactos_yura@gloria.com.pe</p>
                    </div>
                  </div>
                  <div class="col-lg-4 col-3 d-flex justify-content-center align-items-center">
                    <img src="{{ url('images/iconos/gloria.jpg') }}"  class="img-fluid" alt="">
                  </div>
              </a>
          </div>
      </div>
  </section>
  <a href="javascript:void(0)" class="flechaMapa botton botton--normal">Ver más</a>
</div>

@include('frontend.partials.footer')
@include('frontend.partials.modal')
@endsection

@section('scripts')

@stop
