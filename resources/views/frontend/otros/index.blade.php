@extends('frontend.layout.layout')
@section('content')
@include('frontend.partials.menu')

<section class="empresa" style="background-image:url({{ url('images/conoce/otros.jpg') }})">
  <div class="empresa__titulo">
      <div>
        <img src="{{ url('images/iconos/otros-g.png') }}" alt="">
          <h1 class="titulo titulo--grande"><span>otros negocios</span> </h1>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. </p>
          <div class="botones">
            <a href="javascript:void(0)" class="botton botton--blanco bannerTop">Introducción</a>
            <a href="javascript:void(0)" class="botton botton--completo bannerEmpresa">Ver empresas</a>
          </div>
      </div>
  </div>
  <div class="container-fluid ocultoResponsive">
    <div class="row empresa__bottom">
      <div class="col p-0">
        <a href="{{ url('/empresas/alimentos') }}" class="empresa__bottom__item">
          <img src="{{ url('images/iconos/alimentos.png') }}" alt="">
          <h4>Alimentos</h4>
        </a>
      </div>
      <div class="col p-0">
        <a href="{{ url('/empresas/soluciones') }}" class="empresa__bottom__item">
          <img src="{{ url('images/iconos/construccion.png') }}" alt="">
          <h4>Soluciones para la construcción y minería</h4>
        </a>
      </div>
      <div class="col p-0">
        <a href="{{ url('/empresas/agroindustria') }}" class="empresa__bottom__item">
          <img src="{{ url('images/iconos/agroindustria.png') }}" alt="">
          <h4>agroindustria</h4>
        </a>
      </div>
      <div class="col p-0">
        <a href="{{ url('/empresas/papeles') }}" class="empresa__bottom__item">
          <img src="{{ url('images/iconos/papeles.png') }}" alt="">
          <h4>Papeles y Cartones</h4>
        </a>
      </div>
      <div class="col p-0 active">
        <a href="{{ url('/empresas/otros') }}" class="empresa__bottom__item">
          <img src="{{ url('images/iconos/otros.png') }}" alt="">
          <h4>otros negocios</h4>
        </a>
      </div>
    </div>
  </div>
</section>
<section class="secciones p-0 intro">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 p-0">
                <div class="secciones__img secciones--padding">
                <img src="{{ url('images/conoce/otros/intro.png') }}" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-lg-5 d-flex align-items-center">
                <div class="secciones__content">
                    <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>Introducción </span></h2>
                    <div class="">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</p>
                        <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. Phasellus pede arcu, dapibus eu, fermentum et, dapibus sed, urna.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="empresa__content empresaBotom">
  <div class="container">
    <div class="row">
      <div class="col-lg-11 offset-lg-1">
        <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>empresas</span> </h2>
      </div>
      <div class="col-lg-4 col-xl-3 col-md-6">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url({{ url('images/conoce/otros/empresa/1.jpg') }})">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Perú</span>
              <h3>Gloria S.A.</h3>
              <p>Hace 46 años se constituyó Yura S.A., para ser uno de los ejes de desarrollo más importantes de la región sur del país.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
              <a href="#">visitar web</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@include('frontend.partials.footer')
@include('frontend.partials.modal')
@endsection

@section('scripts')

@stop
