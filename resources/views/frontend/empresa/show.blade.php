@extends('frontend.layout.layout')
@section('content')
@include('frontend.partials.menu')

<section class="empresa__interna">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-9 m-auto text-center">
        <div class="empresa__interna__top">
          <a href="{{ url('/empresas') }}" class="botton botton--normal">Regresar</a>
          <div class="logo">
            <img src="{{ url('images/empresa/logo/yura.png') }}" alt="">
          </div>
          <h2>Hace 46 años se constituyó Yura S.A., para ser uno de los ejes de desarrollo más importantes de la región sur del país.</h2>
        </div>
      </div>
      <div class="col-lg-4 p-0">
        <a href="{{ url('images/empresa/interna/2.jpg') }}" data-fancybox="gallery" class="empresa__interna__top__img" style="background-image:url({{ url('images/empresa/interna/1.jpg') }})">
          <div class="zoom">
            <img src="{{ url('images/iconos/zoom.png') }}" alt="">
          </div>
        </a>
      </div>
      <div class="col-lg-4 p-0">
        <a href="{{ url('images/empresa/interna/2.jpg') }}" data-fancybox="gallery" class="empresa__interna__top__img" style="background-image:url({{ url('images/empresa/interna/2.jpg') }})">
          <div class="zoom">
            <img src="{{ url('images/iconos/zoom.png') }}" alt="">
          </div>
        </a>
      </div>
      <div class="col-lg-4 p-0">
        <a href="{{ url('images/empresa/interna/3.jpg') }}" data-fancybox="gallery" class="empresa__interna__top__img" style="background-image:url({{ url('images/empresa/interna/3.jpg') }})">
          <div class="zoom">
            <img src="{{ url('images/iconos/zoom.png') }}" alt="">
          </div>
        </a>
      </div>

    </div>
  </div>
</section>

<section class="empresa__interna__item">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <div class="empresa__interna__item__content">
          <img src="{{ url('images/iconos/web.png') }}" alt="">
          <span>WEB</span>
          <a href="#">www.yura.com.pe</a>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="empresa__interna__item__content">
          <img src="{{ url('images/iconos/location.png') }}" alt="">
          <span>DIRECCIÓN</span>
          <p>Estación Yura, s/n, Distrito de Yura Arequipa - Perú</p>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="empresa__interna__item__content">
          <img src="{{ url('images/iconos/phone.png') }}" alt="">
          <span>Teléfono</span>
          <a href="#">(0051 54) 49-5060</a>
        </div>
      </div>
      <div class="col-lg-3">
        <div class="empresa__interna__item__content">
          <img src="{{ url('images/iconos/mail.png') }}" alt="">
          <span>Enviar mensaje</span>
          <a href="#"  data-toggle="modal" data-target="#modalEscribenos" class="botton botton--normal">Escríbenos</a>
        </div>
      </div>
    </div>
  </div>
</section>


@include('frontend.partials.footer')
@include('frontend.partials.modal')
@endsection

@section('scripts')

@stop
