@extends('frontend.layout.layout')
@section('content')
@include('frontend.partials.menu')

<span id='pestana_vista' valor='empresas'></span>

<div class="empresas">
  <section class="conoce">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="conoce__cambioImagen"  style="background-image:url({{ url( 'images/conoce/neutral.jpg') }})">
            <div class="conoce__card" urlImages="{{ url('images/conoce/alimentos.jpg') }}">
              <div class="conoce__card__content">
                <div class="conoce__card__content__titulo">
                  <img src="images/iconos/alimentos.png" alt="">
                  <h3>Alimentos</h3>
                </div>
              </div>
              <div class="conoce__card__hover d-flex align-items-center justify-content-center" style="background:rgba(5, 118, 184, 0.8);">
                <div class="">
                  <img src="images/iconos/alimentos.png" alt="">
                  <h3>Alimentos</h3>
                  <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
                  <a href="{{ url('/empresas/alimentos') }}" class="botton botton--blanco">Ver más</a>
                </div>
              </div>
            </div>
            <div class="conoce__card" urlImages="{{ url('images/conoce/centroynitrato.jpg') }}">
              <div class="conoce__card__content">
                <div class="conoce__card__content__titulo">
                  <img src="images/iconos/construccion.png" alt="">
                  <h3>Soluciones para la construcción y minería</h3>
                </div>
              </div>
              <div class="conoce__card__hover d-flex align-items-center justify-content-center" style="background:rgba(237, 27, 36, 0.8);">
                <div class="">
                  <img src="images/iconos/construccion.png" alt="">
                  <h3>Soluciones para la construcción y minería</h3>
                  <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
                  <a href="{{ url('/empresas/soluciones') }}" class="botton botton--blanco">Ver más</a>
                </div>
              </div>
            </div>
            <div class="conoce__card" urlImages="{{ url('images/conoce/agroindustria.jpg') }}">
              <div class="conoce__card__content">
                <div class="conoce__card__content__titulo">
                  <img src="{{ url('images/iconos/agroindustria.png') }}" alt="">
                  <h3>agroindustria</h3>
                </div>
              </div>
              <div class="conoce__card__hover d-flex align-items-center justify-content-center" style="background:  rgba(31, 175, 80, 0.8);">
                <div class="">
                  <img src="{{ url('images/iconos/agroindustria.png') }}" alt="">
                  <h3>agroindustria</h3>
                  <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
                  <a href="{{ url('/empresas/agroindustria') }}" class="botton botton--blanco">Ver más</a>
                </div>
              </div>
            </div>
            <div class="conoce__card" urlImages="{{ url('images/conoce/papeles.jpg') }}">
              <div class="conoce__card__content">
                <div class="conoce__card__content__titulo">
                  <img src="{{ url('images/iconos/papeles.png') }}" alt="">
                  <h3>Papeles y Cartones</h3>
                </div>
              </div>
              <div class="conoce__card__hover d-flex align-items-center justify-content-center" style="background:rgba(249, 170, 49, 0.8)">
                <div class="">
                  <img src="{{ url('images/iconos/papeles.png') }}" alt="">
                  <h3>Papeles y Cartones</h3>
                  <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
                  <a href="{{ url('/empresas/papeles') }}" class="botton botton--blanco">Ver más</a>
                </div>
              </div>
            </div>
            <div class="conoce__card" urlImages="{{ url('images/conoce/otros.jpg') }}">
              <div class="conoce__card__content">
                <div class="conoce__card__content__titulo">
                  <img src="{{ url('images/iconos/otros.png') }}" alt="">
                  <h3>Otros Negocios</h3>
                </div>
              </div>
              <div class="conoce__card__hover d-flex align-items-center justify-content-center" style="background:rgba(0, 183, 199, 0.8)">
                <div class="">
                  <img src="{{ url('images/iconos/otros.png') }}" alt="">
                  <h3>Otros Negocios</h3>
                  <p>Por nuestra experiencia en el rubro, nos caracterizamos por la solución integral a los problemas de cada propiedad.</p>
                  <a href="{{ url('/empresas/otros') }}" class="botton botton--blanco">Ver más</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<section class="empresas__carousel">
  <div class="owl-carousel owl-conoce">
    <div class="item">
      <div class="conoce__responsive d-flex align-items-center justify-content-center" style="background-image:url({{ url('images/conoce/alimentos.jpg') }})">
        <div class="conoce__responsive__color" style="background:rgba(5, 118, 184, 0.8);">
        </div>
        <div class="conoce__responsive__texto">
          <div class="">
            <img src="images/iconos/alimentos.png" alt="">
            <h3>Alimentos</h3>
            <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
            <a href="{{ url('/empresas/alimentos') }}" class="botton botton--blanco">Ver más</a>
          </div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="conoce__responsive d-flex align-items-center justify-content-center" style="background-image:url({{ url('images/conoce/centroynitrato.jpg') }})">
        <div class="conoce__responsive__color" style="background:rgba(237, 27, 36, 0.8);">
        </div>
        <div class="conoce__responsive__texto">
          <div class="">
            <img src="images/iconos/construccion.png" alt="">
            <h3>Soluciones para la construcción y minería</h3>
            <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
            <a href="{{ url('/empresas/soluciones') }}" class="botton botton--blanco">Ver más</a>
          </div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="conoce__responsive d-flex align-items-center justify-content-center" style="background-image:url({{ url('images/conoce/agroindustria.jpg') }})">
        <div class="conoce__responsive__color" style="background:  rgba(31, 175, 80, 0.8);">
        </div>
        <div class="conoce__responsive__texto">
          <div class="">
            <img src="{{ url('images/iconos/agroindustria.png') }}" alt="">
            <h3>agroindustria</h3>
            <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
            <a href="{{ url('/empresas/agroindustria') }}" class="botton botton--blanco">Ver más</a>
          </div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="conoce__responsive d-flex align-items-center justify-content-center" style="background-image:url({{ url('images/conoce/papeles.jpg') }})">
        <div class="conoce__responsive__color" style="background:rgba(249, 170, 49, 0.8);">
        </div>
        <div class="conoce__responsive__texto">
          <div class="">
            <img src="{{ url('images/iconos/papeles.png') }}" alt="">
            <h3>Papeles y Cartones</h3>
            <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
            <a href="{{ url('/empresas/papeles') }}"  class="botton botton--blanco">Ver más</a>
          </div>
        </div>
      </div>
    </div>
    <div class="item">
      <div class="conoce__responsive d-flex align-items-center justify-content-center" style="background-image:url({{ url('images/conoce/otros.jpg') }})">
        <div class="conoce__responsive__color" style="background:rgba(0, 183, 199, 0.8)">
        </div>
        <div class="conoce__responsive__texto">
          <div class="">
            <img src="{{ url('images/iconos/otros.png') }}" alt="">
            <h3>Otros Negocios</h3>
            <p>Por nuestra experiencia en el rubro, nos caracterizamos por la solución integral a los problemas de cada propiedad.</p>
            <a href="{{ url('/empresas/otros') }}"  class="botton botton--blanco">Ver más</a>
          </div>
        </div>
      </div>
    </div>
</section>
@include('frontend.partials.modal')
@endsection

@section('scripts')

@stop
