<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-xl-3 d-flex align-items-center justify-content-center">
        <div class="footer__logo">
          <img src="{{ url('images/logo.svg') }}" alt="">
          <a href="email:contactenos@gloria.com.pe">E: contactenos@gloria.com.pe</a>
        </div>
      </div>
      <div class="col-xl-2 offset-xl-4 col-lg-3 offset-lg-2">
        <div class="footer__content">
          <img src="{{ url('images/iconos/siguenos.png') }}" class="icono" alt="">
          <h3><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""> {{ trans('gloria.siguenos') }}</h3>
          <ul>
            <li><i class="fab fa-linkedin-in"></i> <a href="#"> Linkedin</a></li>
            <li><i class="fab fa-youtube"></i> <a href="#"> Youtube</a></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-4 col-xl-3">
        <div class="footer__content footer__content--padding">
          <img src="{{ url('images/iconos/enlaces.png') }}" class="icono" alt="">
          <h3><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""> {{ trans('gloria.enlaces') }}</h3>
          <ul class="mayuscula">
<<<<<<< HEAD
            <li><a href="#"> {{ trans('gloria.r_proveedores') }}</a></li>
            <li><a href="#"> {{ trans('gloria.trabajeconnosotros') }}</a></li>
            <li><a href="#"> {{ trans('gloria.privacidad') }}</a></li>
            <li><a href="{{ url('/contactanos') }}"> {{ trans('gloria.contactanos') }}</a></li>
=======
            <li><a href="https://www.b2mining.com/SRPWeb/login/inicioGloria.do" target="_blank"> REGISTRO DE PROVEEDORES</a></li>
            <li><a href="http://webtiliawebs.com/clientes/in/grupogloria/public/trabajaconnosotros/" target="_blank"> TRABAJE CON NOSOTROS</a></li>
            <li><a href="{{ url('/pdf') }}"> POLÍTICAS DE PRIVACIDAD</a></li>
            <li><a href="{{ url('/contactanos') }}"> CONTÁCTANOS</a></li>
>>>>>>> c2e96db4a38758992f11496e85ff8fbcb980dd0a
          </ul>
        </div>
      </div>
      <div class="col-lg-12 text-center">
        <div class="footer__derechos">
          <h6>© 2020 Grupo Gloria, {{ trans('gloria.derechos') }}.</h6>
          <a href="https://webtilia.com/" target="_blank">By agencia de marketing digital WEBTILIA</a>
        </div>
      </div>
    </div>
  </div>
</footer>
