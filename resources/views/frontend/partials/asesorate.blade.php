<div class="asesorateM" id="asesorateF">
  <div class="container-fluid p-0">
    <div class="row">
      <div class="col-lg-6 col-12 p-0">
        <div class="asesorateM__bg"> 
          <img src="images/iconos/mensaje.png" alt="">  
        </div>
      </div>
      <div class="col-lg-5 col-12 offset-lg-1 p-0 d-flex align-items-center">
      <a href="javascript:void(0)" class="cerrarA cerrar"><img src="images/iconos/cerrar.png" alt=""> </a>
        <div class="asesorateM__formulario">            
          <form>
          <div class="text-center">
            <h2 class="titulo titulo--grande">Asesórate</h2>  
            <p class="parrafo">Déjanos tus datos y el tipo de departamento que estas interesado y nos pondremos en contacto con usted.</p>      
          </div>
            <div class="form-group">
              <label class="alto" for="exampleInputPassword1">Tipo de departamento</label>
              <div class="seleccionar">
                <div id="selector" class="seleccionar__top d-flex align-items-center">
                  <div class="icono">
                    <img src="images/asesorate/1.jpg" class="img-fluid" alt="">
                  </div>
                  <span>DUO tipo 1</span>
                </div>
                <div class="seleccionar__bottom">
                  <a href="#" class="seleccionar__bottom__card">
                    <div class="icono">
                      <img src="images/asesorate/1.jpg" class="img-fluid" alt="">
                    </div>
                    <h3>loft tipo 1</h3>
                  </a>
                  <a href="#" class="seleccionar__bottom__card">
                    <div class="icono">
                      <img src="images/asesorate/1.jpg" class="img-fluid" alt="">
                    </div>
                    <h3>loft tipo 1</h3>
                  </a>
                  <a href="#" class="seleccionar__bottom__card">
                    <div class="icono">
                      <img src="images/asesorate/1.jpg" class="img-fluid" alt="">
                    </div>
                    <h3>loft tipo 1</h3>
                  </a>
                  <a href="#" class="seleccionar__bottom__card">
                    <div class="icono">
                      <img src="images/asesorate/1.jpg" class="img-fluid" alt="">
                    </div>
                    <h3>loft tipo 1</h3>
                  </a>
                  <a href="#" class="seleccionar__bottom__card">
                    <div class="icono">
                      <img src="images/asesorate/1.jpg" class="img-fluid" alt="">
                    </div>
                    <h3>loft tipo 1</h3>
                  </a>
                  <a href="#" class="seleccionar__bottom__card">
                    <div class="icono">
                      <img src="images/asesorate/1.jpg" class="img-fluid" alt="">
                    </div>
                    <h3>loft tipo 1</h3>
                  </a>
                  <a href="#" class="seleccionar__bottom__card">
                    <div class="icono">
                      <img src="images/asesorate/1.jpg" class="img-fluid" alt="">
                    </div>
                    <h3>loft tipo 1</h3>
                  </a>
                  <a href="#" class="seleccionar__bottom__card">
                    <div class="icono">
                      <img src="images/asesorate/1.jpg" class="img-fluid" alt="">
                    </div>
                    <h3>loft tipo 1</h3>
                  </a>
                </div>
              </div>
            </div>
            <div class="form-group">
              <input id="input" type="text" class="input" autocomplete="off" placeholder="Enter value" />
              <span class="label" for="input">Nombre completo *</span>
              <span class="divider"></span>
            </div>
            <div class="form-group">
              <input id="input" type="mail" class="input" autocomplete="off" placeholder="Enter value" />
              <span class="label" for="input">Correo *</span>
              <span class="divider"></span>
            </div>
            <div class="form-group">
              <input id="input" type="mail" class="input" autocomplete="off" placeholder="Enter value" />
              <span class="label" for="input">DNI *</span>
              <span class="divider"></span>
            </div>
            <div class="form-group">
              <input id="input" type="mail" class="input" autocomplete="off" placeholder="Enter value" />
              <span class="label" for="input">Teléfono *</span>
              <span class="divider"></span>
            </div>
            <div class="form-group">
              <div class="check">
                <input type="checkbox" name="category" id="c_1" class="css-checkbox" />
                <label for="c_1" class="css-label">He leído la política de privacidad web.</label>
              </div>
              <div class="check">
                <input type="checkbox" name="category" id="c_2" class="css-checkbox" />
                <label for="c_2" class="css-label">Autorizo a que me envíen publicidad y/o promociones.</label>
              </div>
              <div class="check">
                <input type="checkbox" name="category" id="c_3" class="css-checkbox" />
                <label for="c_3" class="css-label">Autorizo que mi información sea compartida con el resto de empresas del grupo.</label>
              </div>
            </div>
            <a href="gracias.php"  class="buttom buttom--rellenoV">Enviar cotización</a>
            <!-- <button type="submit"  class="buttom buttom__rellenoV">Enviar cotización</button> -->
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
