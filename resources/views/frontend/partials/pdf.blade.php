@extends('frontend.layout.layout')
@section('content')
@include('frontend.partials.menu')

<section class="m-top">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="titulo titulo--mediano mb-5"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>Política de Privacidad</span> </h2>
      </div>
      <div class="col-lg-10 m-auto">
        <p class="parrafo">LA EMPRESA reconoce la importancia que tiene la privacidad de los datos personales de
        nuestros usuarios, por ello, garantizamos la absoluta confidencialidad de los mismos y el
        empleo de estándares de seguridad conforme a lo establecido en la Ley de Protección de
        Datos Personales - Ley N° 29733 y su Reglamento aprobado por el Decreto Supremo N°
        003-2013-JUS (en adelante, “las normas de protección de datos personales”).</p>
        <h3 class="titulo titulo--chico">1. ¿Qué son datos personales?</h3>
        <p>Es toda información que pueda identificar a una persona natural o que la hace identificable
        a través de medios que pueden ser razonablemente utilizados. Por ejemplo: Nombres y
        Apellidos, Documento de Identidad, Estado Civil, entre otros.</p>
        <h3 class="titulo titulo--chico">2. Finalidad del tratamiento de los datos personales</h3>
        <p>Los datos personales recopilados por la empresa, serán tratados con los fines siguientes:</p>
        <ul class="listadoPDF">
          <li>Trabajadores: Registro de la información de postulantes para procesos de selección,
          trabajadores actuales para la gestión de recursos humanos, gestión de seguros
          sociales, comunicaciones internas, bienestar social, antecedentes, licencias, control
          y prevención de seguridad y salud ocupacional, registro histórico de los trabajadores
          derechos y beneficios laborales otorgados. </li>
          <li>Proveedores: Tratamiento de datos personales de proveedores, representantes y
          trabajadores de estos para cumplimiento de relaciones y negociaciones
          contractuales y profesionales que se establezcan con ellos, realizar análisis de
          riesgos crediticos, financieros y comerciales. </li>
          <li>Clientes: Registro de información para la gestión de pedidos, control del proceso
            comercial y servicio post venta. Datos del personal de los contratistas para control
            y cumplimiento de servicios contratados. Gestión comercial, contractual y de
            negociaciones con el objetivo de evaluar y analizar las propuestas de servicios y
            para fines de control y prevención de seguridad.</li>
        </ul>
        <p>Bajo ninguna circunstancia, el tratamiento de los datos personales de nuestros usuarios se
          extenderá a una finalidad distinta a aquellas para las que fueron recopilados.</p>
        <h3 class="titulo titulo--chico">3. Consentimiento</h3>
        <p>El usuario que proporciona sus datos personales ya sea de forma física o electrónica, da
        su consentimiento expreso para que la empresa trate sus datos personales según las
        finalidades descritas en esta Política, así mismo garantiza la exactitud, veracidad y
        actualización de la información proporcionada. </p>
        <p>En caso de proporcionar datos personales inexactos, erróneos o falsos, la empresa no
        podrá cumplir con las finalidades correspondientes.</p>
        <h3 class="titulo titulo--chico">4. Transferencia de información a terceros</h3>
        <p>En base a la finalidad del tratamiento de los datos personales, la empresa podrá transferir
        los datos personales de los usuarios, a sus empresas vinculadas, dentro y fuera del país,
        en tanto el usuario no revoque la presente autorización.
        </p>
        <p>La empresa, en cumplimiento de imperativos legales y/o requerimiento judicial, puede
        encontrarse en la necesidad de compartir los datos personales de sus usuarios con
        juzgados y otras autoridades para cumplir con procedimientos judiciales o de
        requerimientos de una autoridad pública.</p>
        <h3 class="titulo titulo--chico">5. Tratamiento de datos personales de menores de edad</h3>
        <p>La empresa no llevará a cabo el tratamiento de datos personales relativos a menores de 14
        años de edad. En el supuesto que se tenga conocimiento que los datos personales
        recopilados corresponden a un menor de edad, se adoptarán las medidas oportunas para
        su eliminación.</p>
        <p>Sin embargo, excepcionalmente los menores de edad que sean mayores de 14 años podrán
        llenar los formularios web de la empresa, siempre y cuando puedan entender los motivos y
        la finalidad del tratamiento de sus datos personales, salvo exigencia legal en contrario
        </p>
        <h3 class="titulo titulo--chico">6. Plazo de conservación</h3>
        <p>Los datos personales recopilados serán conservados por la empresa por el plazo que se
        considere necesario para el cumplimiento de las finalidades descritas y/o por el tiempo que
        indique alguna norma específica al respecto. Asimismo, los datos personales podrán ser
        cancelados a solicitud del usuario, salvo disposición normativa en contrario.</p>
        <h3 class="titulo titulo--chico">7. Medidas adoptadas para la protección de los datos personales</h3>
        <p>La empresa ha adoptado las medidas de seguridad necesarias exigidas por las normas de
        protección de datos personales y se compromete a tratar los datos personales como
        información confidencial a fin de prevenir e impedir el acceso o divulgación no autorizada y
        asegurar el uso apropiado de la información</p>
        <p>La empresa no se hace responsable sobre el riesgo de pérdida de información de datos
        personales, cuando el usuario realiza la transferencia de información hacia los servidores
        de la empresa a través de su computadora o dispositivo móvil, utilizando conexiones no
        seguras.</p>
        <h3 class="titulo titulo--chico">8. Derechos de información, revocatoria y derechos ARCO</h3>
        <p>La empresa pone a disposición de los usuarios la posibilidad de ejercer los derechos de
        Información, Revocatoria y ARCO (Acceso, Rectificación, Cancelación y Oposición) de
        acuerdo a la legislación vigente. Para ello deberá ingresar a la página web <a href="http://lpdp.centro.com.pe" target="_blank">http://lpdp.centro.com.pe/</a>
         o <a href="http://leydeprotecciondedatospersonales.centro.com.pe" target="_blank">http://leydeprotecciondedatospersonales.centro.com.pe</a> y
        registrar una solicitud de atención adjuntando la información establecida por la ley. Los
        derechos son los siguientes:</p>
        <ul class="listadoPDF">
          <li><strong>Información: </strong> El usuario podrá solicitar información relativa a sus derechos y/o acerca
          de las disposiciones de la Ley de Protección de Datos Personales</li>
          <li><strong> Revocatoria:</strong> El usuario podrá revocar su consentimiento brindado para el tratamiento de sus datos personales en cualquier momento, sin justificación previa y sin que le atribuyan efectos retroactivos.</li>
          <li><strong>Acceso:</strong> El usuario tiene derecho a obtener la información relativa a sus datos personales objeto de tratamiento, así como la forma, motivos y condiciones de su recopilación. </li>
          <li><strong>Rectificación:</strong> El usuario podrá solicitar la corrección de aquellos datos que se encuentren errados o resulten inexactos o falsos.</li>
          <li><strong>Cancelación:</strong> El usuario podrá solicitar la supresión de sus datos personales de las
          bases de datos de la empresa cuando ya no sean necesarios o pertinentes para la
          finalidad para los que fueron recopilados, el plazo para su tratamiento hubiere vencido,
          el usuario haya revocado su consentimiento o cuando no sean tratados conforme a la
          legislación de la materia.</li>
          <li><strong>Oposición:</strong> El usuario tiene derecho a que no se traten sus datos personales o se cese
          su tratamiento cuando no haya prestado su consentimiento para el mismo o cuando
          estos se hubieran obtenido de una fuente de acceso público. Cuando el usuario hubiera
          prestado su consentimiento podrá oponerse al tratamiento de sus datos por motivos
          fundados y legítimos.</li>
        </ul>
        <h3 class="titulo titulo--chico">9. Empleo de cookies</h3>
        <p>Cada vez que el usuario visite este sitio web, o haga uso de alguna de sus aplicaciones, la
        empresa podrá recopilar información mediante el uso de cookies. La información recopilada
        mediante el uso de cookies es considerada información personal de los usuarios y por lo
        tanto debe cumplir lo establecido en la presente política.</p>
        <p>Los usuarios se encuentran en la capacidad de deshabilitar la mayoría de cookies que son
        enviadas a las computadoras y dispositivos móviles por medio del cambio de las
        configuraciones predispuestas para sus navegadores y sistemas operativos. El deshabilitar
        las cookies puede tener como consecuencia que la empresa no ofrezca contenido
        personalizado a sus usuarios</p>
        <h3 class="titulo titulo--chico">10. Blogs, foros y Redes Sociales</h3>
        <p>La empresa no es responsable por el contenido de publicaciones realizadas por usuarios
        dentro de blogs, foros o redes sociales en Internet. Las opiniones registradas en dichos
        medios no reflejan la posición de la empresa y son responsabilidad únicamente del usuario
        que realizó la publicación.</p>
        <p>La empresa se libera de toda responsabilidad que pueda ocasionar el incorrecto
        funcionamiento y/o el inadecuado uso de blog, foros o redes sociales, la falsedad del
        contenido y la ilicitud de la forma en que éste fue obtenido.</p>
        <p>Asimismo, la empresa no se hará responsable de los daños y perjuicios ocasionados por
        dichas publicaciones, así como por la reproducción, distribución, publicación de fotos,
        videos o comentarios que se encuentren bajo la protección de los derechos de propiedad
        intelectual de terceros. </p>
        <h3 class="titulo titulo--chico">11. Cambios a la Política de Privacidad</h3>
        <p>La empresa se reserva el derecho de realizar los cambios que considere pertinentes a su
        Política de Privacidad en cualquier momento sin aviso previo.</p>
        <p>Todo lo expuesto en el presente documento se encuentra en concordancia con la Ley N° 29733 y su correspondiente reglamento.</p>
        <p>Si tiene alguna duda, comentario o pregunta relacionada a la Política de Privacidad o a
alguno de los puntos que forman parte de la misma, le invitamos a escribirnos a <a href="mailto:leydeprotecciondedatos@centro.com.pe">leydeprotecciondedatos@centro.com.pe</a> </p>
      </div>
    </div>
  </div>
</section>

@include('frontend.partials.footer')
@include('frontend.partials.modal')
@endsection

@section('scripts')

@stop
