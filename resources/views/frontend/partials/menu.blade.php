<header class="header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-9 p-0 d-flex align-items-center">
        <div class="header__logo">
          <a href="{{ url('/') }}"><img src="{{ url('images/logo.svg') }}" alt="" class="img-fluid"></a>
        </div>
        <nav class="header__links" id="menu_">
          <ul>
            <li><a href="{{ url('/') }}/{{ trans('gloria.r_inicio') }}" id="inicio">{{ trans('gloria.inicio') }}</a></li>
            <li><a href="{{ url('/') }}/{{ trans('gloria.r_grupo') }}" id="grupo">{{ trans('gloria.elgrupo') }}</a></li>
            <li><a href="{{ url('/empresas') }}" id="empresas">{{ trans('gloria.empresas') }} </a></li>
            <li><a href="{{ url('/novedades') }}" id="novedades">{{ trans('gloria.novedades') }}</a></li>
            <li><a href="{{ url('/sostenibilidad') }}" id="sosteniblidad">{{ trans('gloria.sosteniblidad') }}</a></li>
            <li><a href="{{ url('/contactanos') }}" id="contactanos">{{ trans('gloria.contactanos') }}</a></li>
          </ul>
        </nav>
      </div>
      <div class="col-lg-3 d-flex align-items-center">
        <div class="header__social">
          <div class="idiomas">
            <a href="javascript:void(0)" class="@if($idioma == 'es') active @else '' @endif" idioma="es" active="@if($idioma == 'es'){{ 1 }}@else{{ 0 }}@endif">ESP</a>
            <a href="javascript:void(0)" class="@if($idioma == 'en') active @else '' @endif" idioma="en" active="@if($idioma == 'en'){{ 1 }}@else{{ 0 }}@endif">ING</a>
          </div>
          <div class="redes">
            <div class="">
              <a href="javascript:void(0)"><i class="fab fa-linkedin-in"></i></a>
              <a href="javascript:void(0)"><i class="fab fa-youtube"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- menu responsive -->
<div class="barra-hamburger">
  <a href="javascript:void(0)"><img src="{{ url('images/logo.svg') }}" alt="" class="img-fluid"></a>
</div>
<div class="content content--demo-3">
	<div class="hamburger hamburger--demo-3 js-hover" id="hamburger">
		<div class="hamburger__line hamburger__line--01">
			<div class="hamburger__line-in hamburger__line-in--01"></div>
		</div>
		<div class="hamburger__line hamburger__line--02">
			<div class="hamburger__line-in hamburger__line-in--02"></div>
		</div>
		<div class="hamburger__line hamburger__line--03">
			<div class="hamburger__line-in hamburger__line-in--03"></div>
		</div>
		<div class="hamburger__line hamburger__line--cross01">
			<div class="hamburger__line-in hamburger__line-in--cross01"></div>
		</div>
		<div class="hamburger__line hamburger__line--cross02">
			<div class="hamburger__line-in hamburger__line-in--cross02"></div>
		</div>
	</div>
	<div class="global-menu">
		<div class="global-menu__wrap">
			<a class="global-menu__item global-menu__item--demo-3" href="{{ url('/') }}">Inicio</a>
			<a class="global-menu__item global-menu__item--demo-3" href="{{ url('/grupo') }}">El grupo</a>
			<a class="global-menu__item global-menu__item--demo-3" href="{{ url('/empresas') }}">empresas </a>
			<a class="global-menu__item global-menu__item--demo-3" href="{{ url('/novedades') }}">novedades</a>
			<a class="global-menu__item global-menu__item--demo-3" href="{{ url('/sostenibilidad') }}">sostenibilidad</a>
			<a class="global-menu__item global-menu__item--demo-3" href="{{ url('/contactanos') }}">contáctanos</a>
      <a class="global-menu__item global-menu__item--demo-3" style="width:100px; margin-top:20px !important; margin:auto;" href="#"><img src="{{ url('images/logo.svg') }}" alt="" class="img-fluid"></a>
		</div>
	</div>
	<svg class="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
		<path class="shape-overlays__path"></path>
		<path class="shape-overlays__path"></path>
		<path class="shape-overlays__path"></path>
	</svg>
</div>
