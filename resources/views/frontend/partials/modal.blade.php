<!-- Modal -->
<div class="modal fade modal__video" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><img src="{{ url('images/iconos/cerrar.png') }}" alt=""> </span>
        </button>
        <iframe src="https://www.youtube.com/embed/xNx4x8mRIqc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade modal__escribenos" id="modalEscribenos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><img src="{{ url('images/iconos/cerrar.png') }}" alt=""> </span>
        </button>
        <div class="container">
          <div class="contacto__content row">
              <div class="col-lg-12">
                  <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>Escríbenos un mensaje</span> </h2>
              </div>
              <div class="col-xl-12 col-lg-11 m-auto">
                  <div class="form">
                      <form class="row">
                          <div class="form-group col-lg-6">
                              <input type="text" class="form-control" placeholder="Enter email">
                              <label >Nombre completo</label>
                          </div>
                          <div class="form-group col-lg-6">
                              <input type="mail" class="form-control" placeholder="empresa@dominio.com">
                              <label >Correo</label>
                          </div>
                          <div class="form-group col-lg-6">
                              <input type="text" class="form-control" placeholder="123456789">
                              <label >Teléfono</label>
                          </div>
                          <div class="form-group col-lg-6">
                              <input type="text" class="form-control" placeholder="Asunto">
                              <label>Ingresar asunto</label>
                          </div>
                          <div class="form-group col-lg-12">
                            <label for="exampleInputEmail1" class="label">Tipo de consulta</label>
                              <select id="consulta">
                                  <option value="hide">Seleccionar tipo de consulta</option>
                                  <option value="2010">2010</option>
                                  <option value="2011">2011</option>
                                  <option value="2012">2012</option>
                                  <option value="2013">2013</option>
                                  <option value="2014">2014</option>
                                  <option value="2015">2015</option>
                              </select>
                          </div>
                          <div class="form-group col-lg-12">
                              <textarea name="" class="form-control" placeholder="Saludos..." cols="30" rows="10"></textarea>
                              <label for="exampleInputPassword1">Mensaje</label>
                          </div>
                          <div class="col-lg-12 form__boton">
                            <span>Al enviar acepta los términos y condiciones</span>
                            <button type="submit" class="botton botton--normal">Enviar</button>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
