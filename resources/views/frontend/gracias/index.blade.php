@extends('frontend.layout.layout')

@section('content')

    <section class="gracias d-flex align-items-center">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="gracias__bg">

                    </div>
                </div>
                <div class="col-lg-6 d-flex align-items-center justify-content-center">
                    <div class="gracias__content">
                        <img src="images/iconos/enviar.png" alt="">                    
                        <h1>Mensaje enviado con <span>éxito</span></h1>
                        <p>Nuestros asesores se pondran en contacto con usted</p>                        
                        <a href="" class="buttom buttom--rellenoV">volver al inicio</a>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection

@section('scripts')

@stop
