@extends('frontend.layout.layout')
@section('content')
@include('frontend.partials.menu')

<section class="empresa" style="background-image:url({{ url('images/conoce/agroindustria.jpg') }})">
  <div class="empresa__titulo">
      <div>
        <img src="{{ url('images/iconos/agroindustria-g.png') }}" alt="">
          <h1 class="titulo titulo--grande"><span>agroindustria</span> </h1>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. </p>
          <div class="botones">
            <a href="javascript:void(0)" class="botton botton--blanco bannerTop">Introducción</a>
            <a href="javascript:void(0)" class="botton botton--completo bannerEmpresa">Ver empresas</a>
          </div>
      </div>
  </div>
  <div class="container-fluid ocultoResponsive">
    <div class="row empresa__bottom">
      <div class="col p-0">
        <a href="{{ url('/empresas/alimentos') }}" class="empresa__bottom__item">
          <img src="{{ url('images/iconos/alimentos.png') }}" alt="">
          <h4>Alimentos</h4>
        </a>
      </div>
      <div class="col p-0">
        <a href="{{ url('/empresas/soluciones') }}" class="empresa__bottom__item">
          <img src="{{ url('images/iconos/construccion.png') }}" alt="">
          <h4>Soluciones para la construcción y minería</h4>
        </a>
      </div>
      <div class="col p-0 active">
        <a href="{{ url('/empresas/agroindustria') }}"  class="empresa__bottom__item">
          <img src="{{ url('images/iconos/agroindustria.png') }}" alt="">
          <h4>agroindustria</h4>
        </a>
      </div>
      <div class="col p-0">
        <a href="{{ url('/empresas/papeles') }}" class="empresa__bottom__item">
          <img src="{{ url('images/iconos/papeles.png') }}" alt="">
          <h4>Papeles y Cartones</h4>
        </a>
      </div>
      <div class="col p-0">
        <a href="{{ url('/empresas/otros') }}"  class="empresa__bottom__item">
          <img src="{{ url('images/iconos/otros.png') }}" alt="">
          <h4>otros negocios</h4>
        </a>
      </div>
    </div>
  </div>
</section>
<section class="secciones p-0 intro">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-7 col-lg-6 p-0">
                <div class="secciones__img secciones--padding">
                <img src="{{ url('images/conoce/agroindustria/intro.png') }}" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 d-flex align-items-center">
                <div class="secciones__content">
                    <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>Introducción </span></h2>
                    <div class="">
                        <p>Nuestras actividades se desarrollan en los sectores de lácteos y alimentos, en cemento, papeles, agroindustria, transporte y servicios; todos ellos focalizados en la calidad del producto o servicio que se entrega al consumidor en todo momento. </p>
                        <p>El crecimiento y fortalecimiento estratégico del Grupo Gloria se sustenta a base del liderazgo de sus marcas en los mercados donde operan. La variedad y calidad de los productos que fabrica y comercializa, aunado a la eficiente capacidad de distribución y transporte para llegar a todos los mercados que abastece, le permiten al Grupo Gloria generar sinergias que garantizan una estructura diversificada de negocios, capaz de desempeñarse con éxito en un entorno altamente competitivo.</p>
                        <p>Es así que, con calidad, innovación y competitividad ayudamos a elevar los estándares de la región y contribuimos con nuestro esfuerzo a apoyar el desarrollo de la economía del Perú.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="empresa__content empresaBotom">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 offset-xl-1 col-lg-5 col-md-6">
        <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>empresas</span> </h2>
      </div>
      <div class="col-lg-7 col-xl-4 col-md-6">
        <div class="empresa__content__filtro">
          <form class="row d-flex align-items-center" action="index.html" method="post">
            <div class="col-lg-3">
              <label for="">Seleccionar país:</label>
            </div>
            <div class="col-lg-4">
              <select id="pais">
                  <option value="hide"> Todos</option>
                  <option value="2010">2010</option>
                  <option value="2011">2011</option>
                  <option value="2012">2012</option>
                  <option value="2013">2013</option>
                  <option value="2014">2014</option>
                  <option value="2015">2015</option>
              </select>
            </div>
            <div class="col-lg-5 p-0">
              <div class="buscador">
                <input type="text" name="" placeholder="Buscar Empresa" value="">
                <button type="button" name="button" class="buscar"><i class="fas fa-search"></i></button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="col-lg-4 col-xl-3 col-md-6">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url({{ url('images/conoce/agroindustria/empresa/1.jpg') }})">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Perú</span>
              <h3>Casa Grande S.A.A.</h3>
              <p>Hace 46 años se constituyó Yura S.A., para ser uno de los ejes de desarrollo más importantes de la región sur del país.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
              <a href="#">visitar web</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-xl-3 col-md-6">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url( {{  url('images/conoce/agroindustria/empresa/2.jpg') }} )">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Perú</span>
              <h3>Cartavio S.A.A.</h3>
              <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-xl-3 col-md-6">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url( {{ url('images/conoce/agroindustria/empresa/3.jpg') }} )">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Perú</span>
              <h3>Chiquitoy S.A.</h3>
              <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
              <a href="#">visitar web</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-xl-3 col-md-6">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url( {{ url('images/conoce/agroindustria/empresa/4.jpg') }} )">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Perú</span>
              <h3>Sintuco S.A</h3>
              <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-xl-3 col-md-6">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url( {{ url('images/conoce/agroindustria/empresa/5.jpg') }} )">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Bolivia</span>
              <h3>San Jacinto S.A.A.</h3>
              <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
              <a href="#">visitar web</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-xl-3 col-md-6">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url( {{ url('images/conoce/agroindustria/empresa/6.jpg') }} )">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Argentina</span>
              <h3>Prosal S.A.</h3>
              <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-xl-3 col-md-6">
        <div class="empresa__content__card">
          <div class="empresa__content__card__img" style="background-image:url( {{ url('images/conoce/agroindustria/empresa/7.jpg') }} )">
              <div class="logo__empresa">
                <img src="{{ url('images/logo/1.png') }}" alt="">
              </div>
          </div>
          <div class="empresa__content__card__texto">
            <div class="">
              <span>Ecuador</span>
              <h3>La Troncal S.A.</h3>
              <p>Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci. Aenean dignissim pellentesque felis.</p>
            </div>
            <div class="botones">
              <a href="{{ url('empresa/interna') }}">Ver más</a>
              <a href="#">visitar web</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@include('frontend.partials.footer')
@include('frontend.partials.modal')
@endsection

@section('scripts')

@stop
