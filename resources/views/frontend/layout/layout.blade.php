<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="theme-color" content="#0e1126" />
<link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&display=swap" rel="stylesheet">
<!-- <meta name="description" content="AUDACITY, UN DESARROLLO QUE TE ELEVA A OTRA ALTURA, LA VERTICALIZACIÓN DEL FUTURO."> -->
<title>Grupo Gloria</title>
<link rel="icon" href="{{ asset('images/favicon.png')}}"/>
<link rel="stylesheet" href="{{ url('css/app.css?v=1.1') }}">
</head>
<body>
<!--<div class="contenedor_carga">
  <div class="contenedor_carga__giro"></div>
</div>-->
@yield('content')


<script src="{{ url('js/app.js') }}"></script>
@yield('scripts')
</body>
</html>
