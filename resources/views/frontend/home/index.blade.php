@extends('frontend.layout.layout')
@section('content')
@include('frontend.partials.menu')
<span id='pestana_vista' valor='inicio'></span>
<a class="scroll-down hash-link" href="#intro" title="Gloria">
  <i>
    <svg class="arrow" width="7" height="5" viewBox="0 0 7 5" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><path id="arrow" fill="#FFF" fill-rule="nonzero" d="M0 0l3.5 5L7 0z"></path></g></svg>
    <svg class="circle" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
      <g class="circle-wrap" fill="none" stroke="#fff" stroke-width="1" stroke-linejoin="round" stroke-miterlimit="10">
          <circle cx="12" cy="12" r="10.5"></circle>
      </g>
    </svg>
  </i>
</a>
<div class="owl-carousel slider owl-slider" id="inicio">


@foreach( $banner as $banner_ )
  <div class="item">
    <div class="slider__content">
      <div class="single-slider zoom" style="background-image:url('{{ url('storage') }}/{{ $banner_->imagen }}');">
      </div>
      <div class="slider__content__item d-flex align-items-center">
        <div class="centrado">
          <h2 class="slider__content__item__titulo">@if($idioma == 'es'){{ $banner_->titulo }}@else{{ $banner_->titulo_ingles }}@endif <span>@if($idioma == 'es'){{ $banner_->titulo_adicional }}@else{{ $banner_->titulo_adicional_ingles }}@endif</span></h2>

          @if ( $banner_->url )
          <a href="javascript:void(0)" class="botton botton--blanco">{{ trans('gloria.vermas') }}</a>
          @endif
        </div>
      </div>
    </div>
  </div>
@endforeach

<!--
  <div class="item">
    <div class="slider__content">
      <div class="single-slider zoom" style="background-image:url('images/slider/2.jpg');">
      </div>
      <div class="slider__content__item d-flex align-items-center">
        <div class="centrado">
          <h2 class="slider__content__item__titulo">líderes en la industria <span>láctea peruana</span> </h2>
          <a href="javascript:void(0)" class="botton botton--blanco">{{ trans('gloria.vermas') }}</a>
        </div>
      </div>
    </div>
  </div>
  <div class="item">
    <div class="slider__content">
      <div class="single-slider zoom" style="background-image:url('images/slider/3.jpg');">
      </div>
      <div class="slider__content__item d-flex align-items-center">
        <div class="centrado">
          <img src="{{ url('images/slider/icon3.png') }}" class="icono" alt="">
          <h2 class="slider__content__item__titulo">Fomento ganadero <span>sostenible</span></h2>
          <a href="javascript:void(0)" class="botton botton--blanco">{{ trans('gloria.vermas') }}</a>
        </div>
      </div>
    </div>
  </div>

-->
</div>


<!--  STAR CONOCE -->

<section class="conoce seccionPage">

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <h2 class="titulo titulo--mediano" data-aos="fade-down"><img src="images/iconos/cruz-rojo.png" alt=""> <span>{{ trans('gloria.conoce') }}</span> {{ trans('gloria.empresa') }}</h2>
        <div class="owl-carousel owl-conoce">
          <div class="item">
            <div class="conoce__responsive d-flex align-items-center justify-content-center" style="background-image:url({{ url('images/conoce/alimentos.jpg') }})">
              <div class="conoce__responsive__color" style="background:rgba(5, 118, 184, 0.8);">
              </div>
              <div class="conoce__responsive__texto">
                <div class="">
                  <img src="images/iconos/alimentos.png" alt="">
                  <h3>Alimentos</h3>
                  <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
                  <a href="{{ url('/empresas/alimentos') }}" class="botton botton--blanco">{{ trans('gloria.vermas') }}</a>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="conoce__responsive d-flex align-items-center justify-content-center" style="background-image:url({{ url('images/conoce/centroynitrato.jpg') }})">
              <div class="conoce__responsive__color" style="background:rgba(237, 27, 36, 0.8);">
              </div>
              <div class="conoce__responsive__texto">
                <div class="">
                  <img src="images/iconos/construccion.png" alt="">
                  <h3>Soluciones para la <br> construcción y minería</h3>
                  <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
                  <a href="{{ url('/empresas/soluciones') }}" class="botton botton--blanco">{{ trans('gloria.vermas') }}</a>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="conoce__responsive d-flex align-items-center justify-content-center" style="background-image:url({{ url('images/conoce/agroindustria.jpg') }})">
              <div class="conoce__responsive__color" style="background:  rgba(31, 175, 80, 0.8);">
              </div>
              <div class="conoce__responsive__texto">
                <div class="">
                  <img src="{{ url('images/iconos/agroindustria.png') }}" alt="">
                  <h3>agroindustria</h3>
                  <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
                  <a href="{{ url('/empresas/agroindustria') }}" class="botton botton--blanco">{{ trans('gloria.vermas') }}</a>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="conoce__responsive d-flex align-items-center justify-content-center" style="background-image:url({{ url('images/conoce/papeles.jpg') }})">
              <div class="conoce__responsive__color" style="background:rgba(249, 170, 49, 0.8);">
              </div>
              <div class="conoce__responsive__texto">
                <div class="">
                  <img src="{{ url('images/iconos/papeles.png') }}" alt="">
                  <h3>Papeles y Cartones</h3>
                  <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
                  <a href="{{ url('/empresas/papeles') }}"  class="botton botton--blanco">{{ trans('gloria.vermas') }}</a>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="conoce__responsive d-flex align-items-center justify-content-center" style="background-image:url({{ url('images/conoce/otros.jpg') }})">
              <div class="conoce__responsive__color" style="background:rgba(0, 183, 199, 0.8)">
              </div>
              <div class="conoce__responsive__texto">
                <div class="">
                  <img src="{{ url('images/iconos/otros.png') }}" alt="">
                  <h3>Otros Negocios</h3>
                  <p>Por nuestra experiencia en el rubro, nos caracterizamos por la solución integral a los problemas de cada propiedad.</p>
                  <a href="{{ url('/empresas/otros') }}"  class="botton botton--blanco">{{ trans('gloria.vermas') }}</a>
                </div>
              </div>
            </div>
          </div>
      </div>
        <div class="conoce__cambioImagen" data-aos="fade-up" style="background-image:url( {{ url('images/conoce/neutral.jpg') }} )">
          <div class="conoce__card" urlImages="{{ url('images/conoce/alimentos.jpg') }}">
            <div class="conoce__card__content">
              <div class="conoce__card__content__titulo">
                <img src="images/iconos/alimentos.png" alt="">
                <h3>Alimentos</h3>
              </div>
            </div>
            <div class="conoce__card__hover d-flex align-items-center justify-content-center" style="background:rgba(5, 118, 184, 0.8);">
              <div class="">
                <img src="images/iconos/alimentos.png" alt="">
                <h3>Alimentos</h3>
                <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
                <a href="{{ url('/empresas/alimentos') }}"  class="botton botton--blanco">{{ trans('gloria.vermas') }}</a>
              </div>
            </div>
          </div>
          <div class="conoce__card" urlImages="{{ url('images/conoce/centroynitrato.jpg') }}">
            <div class="conoce__card__content">
              <div class="conoce__card__content__titulo">
                <img src="images/iconos/construccion.png" alt="">
                <h3>Soluciones para la <br> construcción y minería</h3>
            </div>
            </div>
            <div class="conoce__card__hover d-flex align-items-center justify-content-center" style="background:rgba(237, 27, 36, 0.8);">
              <div class="">
                <img src="images/iconos/construccion.png" alt="">
                <h3>Soluciones para la <br> construcción y minería</h3>
                <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
                <a href="{{ url('/empresas/soluciones') }}" class="botton botton--blanco">{{ trans('gloria.vermas') }}</a>
              </div>
            </div>
          </div>
          <div class="conoce__card" urlImages="{{ url('images/conoce/agroindustria.jpg') }}">
            <div class="conoce__card__content">
              <div class="conoce__card__content__titulo">
                <img src="{{ url('images/iconos/agroindustria.png') }}" alt="">
                <h3>agroindustria</h3>
              </div>
            </div>
            <div class="conoce__card__hover d-flex align-items-center justify-content-center" style="background:  rgba(31, 175, 80, 0.8);">
              <div class="">
                <img src="{{ url('images/iconos/agroindustria.png') }}" alt="">
                <h3>agroindustria</h3>
                <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
                <a href="{{ url('/empresas/agroindustria') }}" class="botton botton--blanco">{{ trans('gloria.vermas') }}</a>
              </div>
            </div>
          </div>
          <div class="conoce__card" urlImages="{{ url('images/conoce/papeles.jpg') }}">
            <div class="conoce__card__content">
              <div class="conoce__card__content__titulo">
                <img src="{{ url('images/iconos/papeles.png') }}" alt="">
                <h3>Papeles y Cartones</h3>
              </div>
            </div>
            <div class="conoce__card__hover d-flex align-items-center justify-content-center" style="background:rgba(249, 170, 49, 0.8)">
              <div class="">
                <img src="{{ url('images/iconos/papeles.png') }}" alt="">
                <h3>Papeles y Cartones</h3>
                <p>Donec nec justo eget felis facilisis fermentum aliquam porttitor mauris sit amet orci. Aenean</p>
                <a href="{{ url('/empresas/papeles') }}" class="botton botton--blanco">{{ trans('gloria.vermas') }}</a>
              </div>
            </div>
          </div>
          <div class="conoce__card" urlImages="{{ url('images/conoce/otros.jpg') }}">
            <div class="conoce__card__content">
              <div class="conoce__card__content__titulo">
                <img src="{{ url('images/iconos/otros.png') }}" alt="">
                <h3>Otros Negocios</h3>
              </div>
            </div>
            <div class="conoce__card__hover d-flex align-items-center justify-content-center" style="background:rgba(0, 183, 199, 0.8)">
              <div class="">
                <img src="{{ url('images/iconos/otros.png') }}" alt="">
                <h3>Otros Negocios</h3>
                <p>Por nuestra experiencia en el rubro, nos caracterizamos por la solución integral a los problemas de cada propiedad.</p>
                <a href="{{ url('/empresas/otros') }}"  class="botton botton--blanco">{{ trans('gloria.vermas') }}</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--  END CONOCE -->


<section class="objetivos objetivos--inicio">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 offset-lg-1">
        <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" class="img-fluid" alt=""><span>{{ trans('gloria.objetivos') }}</span><br> {{ trans('gloria.desarrollosostenible') }}</h2>
        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>
      </div>
      <div class="col-lg-11 offset-lg-1">
        <div class="owl-carousel owl-objetivos">
            <div class="item">
              <img src="{{ url('images/objetivos/1.jpg') }}" alt="">
            </div>
            <div class="item">
              <img src="{{ url('images/objetivos/2.jpg') }}" alt="">
            </div>
            <div class="item">
              <img src="{{ url('images/objetivos/3.jpg') }}" alt="">
            </div>
            <div class="item">
              <img src="{{ url('images/objetivos/4.jpg') }}" alt="" >
            </div>
            <div class="item">
              <img src="{{ url('images/objetivos/5.jpg') }}" alt="">
            </div>
            <div class="item">
              <img src="{{ url('images/objetivos/6.jpg') }}" alt="">
            </div>
            <div class="item">
              <img src="{{ url('images/objetivos/7.jpg') }}" alt="">
            </div>
            <div class="item">
              <img src="{{ url('images/objetivos/8.jpg') }}" alt="">
            </div>
            <div class="item">
              <img src="{{ url('images/objetivos/9.jpg') }}" alt="">
            </div>
            <div class="item">
              <img src="{{ url('images/objetivos/10.jpg') }}" alt="">
            </div>
            <div class="item">
              <img src="{{ url('images/objetivos/11.jpg') }}" alt="">
            </div>
            <div class="item">
              <img src="{{ url('images/objetivos/12.jpg') }}" alt="">
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!--  STAR VIDEO -->
<div class="video"  data-aos="fade-up">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 offset-lg-1">
        <div class="video__content d-flex align-items-center">
          <h2 data-aos="fade-right" class="titulo titulo--grande"><img src="{{ url('images/iconos/cruz-blanco.png') }}" alt=""> <span>Grupo gloria,</span> {{ trans('gloria.crecimiento') }} </h2>
        </div>
      </div>
      <div class="col-lg-6">
        <a href="javascript:void(0)" data-toggle="modal" data-target="#modalVideo" class="play">
          <div class="icono" data-aos="fade-left">
            <div class="icono__circulo">
              <i class="fas fa-play"></i>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>
</div>
<!--  END VIDEO -->

<!--  STAR DONDE ESTAMOS -->
<section class="donde">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-4 d-flex align-items-center">
        <div class="donde__texto" data-aos="flip-right" data-aos-duration="1000">
          <img src="{{ url('images/iconos/pin.png') }}" data-aos="zoom-out" data-aos-duration="3000" class="pin">
          <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" ><span>{{ trans('gloria.donde') }}</span> {{ trans('gloria.estamos') }}</h2>
          <div class="">
            <p>{{ trans('gloria.negocios') }}</p>
          </div>
          <a href="{{ url('/contactanos') }}" class="botton botton--normal">{{ trans('gloria.btn_contactenos') }}</a>
        </div>
      </div>

      <div class="col-lg-8">
        <img src="{{ url('images/mundo.png') }}" data-aos="fade"  data-aos-duration="2000" class="img-fluid">
      </div>
    </div>
  </div>
</section>

<!--  END DONDE ESTAMOS -->

<!--  STAR NOVEDADES -->
<section class="novedades">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6">
        <h2 data-aos="fade-right" class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>{{ trans('gloria.novedades') }}</span> </h2>
      </div>
      <div class="col-lg-5">
        <div class="novedades__botton"><a href="{{ url('/novedades') }}" data-aos="fade-left" class="botton botton--normal">{{ trans('gloria.btn_ver_novedades') }}</a></div>
      </div>
      <div class="col-lg-11 offset-lg-1">
        <div class="owl-carousel owl-novedades" data-aos="fade-up" data-aos-duration="1000">
          <div class="item">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/1.jpg')">
              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>El Grupo Gloria adquirió la mayoría de Acciones de Agroindustrial Casa Grande. S.A.</h3>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/2.jpg')">
              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>El Grupo Gloria adquiere Lechera Andina S.A. de Ecuador.</h3>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/3.jpg')">

              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Título ficticio Yura es una de las 10 empresas más competitivas de América Latina</h3>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/4.jpg')">

              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Gloria S.A. es una de las 100 Empresas más Competitivas de América Latina</h3>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/2.jpg')">

              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Título ficticio Yura es una de las 10 empresas más competitivas de América Latina</h3>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/4.jpg')">
              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Gloria S.A. es una de las 100 Empresas más Competitivas de América Latina</h3>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/4.jpg')">
              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Gloria S.A. es una de las 100 Empresas más Competitivas de América Latina</h3>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/4.jpg')">
              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Gloria S.A. es una de las 100 Empresas más Competitivas de América Latina</h3>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/4.jpg')">
              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Gloria S.A. es una de las 100 Empresas más Competitivas de América Latina</h3>
              </div>
            </a>
          </div>
        </div>
        <!-- <div class="progress">
          <div class="progress-bar" id='progressbar__novedades' role="progressbar" style="width:  1% "aria-valuemax="100"></div>
        </div> -->
      </div>
    </div>
  </div>
</section>
<!--  END NOVEDADES -->

  @include('frontend.partials.footer')
  @include('frontend.partials.modal')
@endsection

@section('scripts')

@stop
