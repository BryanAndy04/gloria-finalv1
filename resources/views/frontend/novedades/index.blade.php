@extends('frontend.layout.layout')
@section('content')
@include('frontend.partials.menu')
<span id='pestana_vista' valor='novedades'></span>

<section class="banner" style="background-image:url( {{ url('images/banner/banner-novedades.jpg') }} )">
    <div class="banner__titulo">
        <div>
            <h1>novedades</h1>
            <p>Conoce todo lo que acontece grupo gloria.</p>
        </div>
    </div>
    <a class="scroll-down hash-link" href="#intro" title="Gloria">
  		<i>
  			<svg class="arrow" width="7" height="5" viewBox="0 0 7 5" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><path id="arrow" fill="#FFF" fill-rule="nonzero" d="M0 0l3.5 5L7 0z"></path></g></svg>
  			<svg class="circle" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
          <g class="circle-wrap" fill="none" stroke="#fff" stroke-width="1" stroke-linejoin="round" stroke-miterlimit="10">
              <circle cx="12" cy="12" r="10.5"></circle>
          </g>
        </svg>
  		</i>
  	</a>
</section>

<section class="secciones ultimasNov seccionPage">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 ">
              <div class="owl-carousel owl-ultimasNov">
                <div class="item">
                  <div class="row" style="padding:0">
                    <div class="secciones__img col-xl-7 col-lg-7 col-12">
                      <img src="{{ url('images/novedades/ultimas/1.jpg') }}" class="img-fluid" alt="">
                    </div>
                    <div class="secciones__content col-xl-5 col-lg-5 col-12 d-flex align-items-center">
                        <div class="">
                          <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>Últimas novedades</span></h2>
                          <div class="secciones__content__item">
                              <span>27,junio 2018</span>
                              <h5>El Grupo Gloria adquirió la mayoría de Acciones de Agroindustrial Casa Grande. S.A.</h5>
                              <p>El 29 de enero de 2006 se cerró la Oferta Pública de Adquisición de acciones de Casa Grande habiendo obtenido el Grupo, a través de la empresa Corporación Azucarera del Perú S.A.A., el 57% de las acciones logrando de esta manera la mayoría accionaria.</p>
                              <p>El Grupo tiene previsto invertir en Casa Grande US $ 60 millones en los próximos cinco años, los cuales se utilizarían exclusivamente para mejorar las condiciones industriales de la empresa y sembrar los campos que están disponibles.</p>
                              <a href="{{ url('/novedades/interna') }}" class="botton botton--normal">Leer más</a>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="row" style="padding:0">
                    <div class="secciones__img col-xl-7 col-lg-7 col-12">
                      <img src="{{ url('images/novedades/ultimas/1.jpg') }}" class="img-fluid" alt="">
                    </div>
                    <div class="secciones__content col-xl-5 col-lg-5 col-12 d-flex align-items-center">
                        <div class="">
                          <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>Últimas novedades</span></h2>
                          <div class="secciones__content__item">
                              <span>27,junio 2018</span>
                              <h5>El Grupo Gloria adquirió la mayoría de Acciones de Agroindustrial Casa Grande. S.A.</h5>
                              <p>El 29 de enero de 2006 se cerró la Oferta Pública de Adquisición de acciones de Casa Grande habiendo obtenido el Grupo, a través de la empresa Corporación Azucarera del Perú S.A.A., el 57% de las acciones logrando de esta manera la mayoría accionaria.</p>
                              <p>El Grupo tiene previsto invertir en Casa Grande US $ 60 millones en los próximos cinco años, los cuales se utilizarían exclusivamente para mejorar las condiciones industriales de la empresa y sembrar los campos que están disponibles.</p>
                              <a href="{{ url('/novedades/interna') }}" class="botton botton--normal">Leer más</a>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="row" style="padding:0">
                    <div class="secciones__img col-xl-7 col-lg-7 col-12">
                      <img src="{{ url('images/novedades/ultimas/1.jpg') }}" class="img-fluid" alt="">
                    </div>
                    <div class="secciones__content col-xl-5 col-lg-5 col-12 d-flex align-items-center">
                        <div class="">
                          <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>Últimas novedades</span></h2>
                          <div class="secciones__content__item">
                              <span>27,junio 2018</span>
                              <h5>El Grupo Gloria adquirió la mayoría de Acciones de Agroindustrial Casa Grande. S.A.</h5>
                              <p>El 29 de enero de 2006 se cerró la Oferta Pública de Adquisición de acciones de Casa Grande habiendo obtenido el Grupo, a través de la empresa Corporación Azucarera del Perú S.A.A., el 57% de las acciones logrando de esta manera la mayoría accionaria.</p>
                              <p>El Grupo tiene previsto invertir en Casa Grande US $ 60 millones en los próximos cinco años, los cuales se utilizarían exclusivamente para mejorar las condiciones industriales de la empresa y sembrar los campos que están disponibles.</p>
                              <a href="{{ url('/novedades/interna') }}" class="botton botton--normal">Leer más</a>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</section>
<section class="novedades novedades--width">
  <div class="container">
    <div class="row">
      <div class="col-xl-3 col-lg-3">
        <div class="novedades__filtro sticky">
          <form class="" action="index.html" method="post">
            <div class="form-group">
              <input type="text" name="" placeholder="Buscar..." value="">
              <button type="button" name="button" class="buscar"><i class="fas fa-search"></i></button>
            </div>
            <div class="form-group">
              <label for="">Filtrar por año</label>
              <select id="anio">
                  <option value="hide"> Todos</option>
                  <option value="2010">2010</option>
                  <option value="2011">2011</option>
                  <option value="2012">2012</option>
                  <option value="2013">2013</option>
                  <option value="2014">2014</option>
                  <option value="2015">2015</option>
              </select>
            </div>
            <div class="form-group">
              <label for="">Filtrar por mes</label>
              <select id="mes">
                  <option value="hide"> Todos</option>
                  <option value="2010">Enero</option>
                  <option value="2011">Febrero</option>
                  <option value="2012">Marzo</option>
                  <option value="2013">Abril</option>
                  <option value="2014">Mayo</option>
                  <option value="2015">Junio</option>
                  <option value="2015">Julio</option>
                  <option value="2015">Agosto</option>
              </select>
            </div>
          </form>
        </div>
      </div>
      <div class="col-xl-9 col-lg-9">
        <div class="row">
          <div class="col-lg-4 col-md-6">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/1.jpg')">
              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>El Grupo Gloria adquirió la mayoría de Acciones de Agroindustrial Casa Grande. S.A.</h3>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-md-6">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/2.jpg')">
              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>El Grupo Gloria adquiere Lechera Andina S.A. de Ecuador.</h3>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-md-6">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/3.jpg')">

              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Título ficticio Yura es una de las 10 empresas más competitivas de América Latina</h3>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-md-6">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/4.jpg')">

              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Gloria S.A. es una de las 100 Empresas más Competitivas de América Latina</h3>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-md-6">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/2.jpg')">
              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>El Grupo Gloria adquiere Lechera Andina S.A. de Ecuador.</h3>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-md-6">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/1.jpg')">
              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>El Grupo Gloria adquirió la mayoría de Acciones de Agroindustrial Casa Grande. S.A.</h3>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-md-6">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/1.jpg')">
              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>El Grupo Gloria adquirió la mayoría de Acciones de Agroindustrial Casa Grande. S.A.</h3>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-md-6">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/2.jpg')">
              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>El Grupo Gloria adquiere Lechera Andina S.A. de Ecuador.</h3>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-md-6">
            <a href="{{ url('/novedades/interna') }}"  class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/3.jpg')">

              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Título ficticio Yura es una de las 10 empresas más competitivas de América Latina</h3>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-md-6">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/4.jpg')">

              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Gloria S.A. es una de las 100 Empresas más Competitivas de América Latina</h3>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-md-6">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/3.jpg')">

              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Título ficticio Yura es una de las 10 empresas más competitivas de América Latina</h3>
              </div>
            </a>
          </div>
          <div class="col-lg-4 col-md-6">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('images/novedades/4.jpg')">

              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Gloria S.A. es una de las 100 Empresas más Competitivas de América Latina</h3>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@include('frontend.partials.footer')
@include('frontend.partials.modal')
@endsection

@section('scripts')

@stop
