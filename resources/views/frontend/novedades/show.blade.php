@extends('frontend.layout.layout')
@section('content')
@include('frontend.partials.menu')

<section class="novedades__interna">
  <section class="novedades__interna__top">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 text-center m-auto">
          <div class="item">
            <a href="{{ url('/novedades') }}" class="botton botton--normal">Regresa</a>
            <h2>El Grupo Gloria adquirió la mayoría de Acciones de Agroindustrial Casa Grande. S.A.</h2>
            <span>27,junio 2018</span>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="novedades__interna__bottom">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 m-auto p-0">
          <div class="parrafoCentrado">
            <img src="{{ url('images/iconos/raya-cuadrado.png') }}" class="cuadrado" alt="">
            <p><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""> El 29 de enero de 2006 se cerró la Oferta Pública de Adquisición de acciones de Casa Grande habiendo obtenido el Grupo, a través de la empresa Corporación Azucarera del Perú S.A.A., el 57% de las acciones logrando de esta manera la mayoría accionaria. </p>
          </div>
        </div>
        <div class="col-lg-10 m-auto">
          <div class="owl-carousel owl-interna">
            <div class="item">
              <img src="{{url('images/novedades/interna/1.jpg') }}" alt="" class="img-fluid">
            </div>
            <div class="item">
              <img src="{{url('images/novedades/interna/1.jpg') }}" alt="" class="img-fluid">
            </div>
            <div class="item">
              <img src="{{url('images/novedades/interna/1.jpg') }}" alt="" class="img-fluid">
            </div>
          </div>
        </div>
        <div class="col-lg-6 m-auto p-0">
          <div class="contenido">
            <p>El Grupo tiene previsto invertir en Casa Grande US $ 60 millones en los próximos cinco años, los cuales se utilizarían exclusivamente para mejorar las condiciones industriales de la empresa y sembrar los campos que están disponibles.</p>
            <p>Casa Grande fue, en el siglo pasado, la primera y más grande industria azucarera del Perú y está ubica en el departamento de La Libertad. Cuenta con una extensión de más de 30,000 hectáreas de terreno para cultivo, de las cuales sólo un tercio se encuentra en producción.</p>
          </div>
          <div class="flechas">
            <a href="#"><img src="{{ url('images/iconos/flechaRojo.png') }}" alt=""> anterior</a>
            <a href="#">siguiente <img src="{{ url('images/iconos/flechaRojo.png') }}" alt=""></a>
          </div>
          <div class="compartir">
            <span>COMPARTIR</span>
            <ul>
              <li>
                <a href="#"><i class="fab fa-facebook-f"></i></a>
              </li>
              <li>
                <a href="#"><i class="fab fa-twitter"></i></a>
              </li>
              <li>
                <a href="#"><i class="fab fa-linkedin-in"></i></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
<section class="novedades">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 col-lg-6">
        <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>Novedades Relacionadas</span> </h2>
      </div>
      <div class="col-md-6 col-lg-5">
        <div class="novedades__botton">
          <a href="{{ url('/novedades') }}" class="botton botton--normal">Ver novedades</a>
        </div>
      </div>
      <div class="col-lg-11 offset-lg-1">
        <div class="owl-carousel owl-novedades">
          <div class="item">
            <a href="{{ url('/novedades/interna') }}" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('../images/novedades/1.jpg')">

              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>El Grupo Gloria adquirió la mayoría de Acciones de Agroindustrial Casa Grande. S.A.</h3>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="javascript:void(0)" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('../images/novedades/2.jpg')">
              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>El Grupo Gloria adquiere Lechera Andina S.A. de Ecuador.</h3>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="javascript:void(0)" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('../images/novedades/3.jpg')">

              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Título ficticio Yura es una de las 10 empresas más competitivas de América Latina</h3>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="javascript:void(0)" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('../images/novedades/4.jpg')">

              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Gloria S.A. es una de las 100 Empresas más Competitivas de América Latina</h3>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="javascript:void(0)" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('../images/novedades/2.jpg')">

              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Título ficticio Yura es una de las 10 empresas más competitivas de América Latina</h3>
              </div>
            </a>
          </div>
          <div class="item">
            <a href="javascript:void(0)" class="novedades__card">
              <div class="novedades__card__img" style="background-image:url('../images/novedades/4.jpg')">

              </div>
              <div class="novedades__card__content">
                  <span>27, junio 2018</span>
                  <h3>Gloria S.A. es una de las 100 Empresas más Competitivas de América Latina</h3>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


@include('frontend.partials.footer')
@include('frontend.partials.modal')
@endsection

@section('scripts')

@stop
