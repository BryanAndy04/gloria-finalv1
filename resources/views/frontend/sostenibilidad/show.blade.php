@extends('frontend.layout.layout')
@section('content')
@include('frontend.partials.menu')

<section class="novedades__interna">
  <section class="novedades__interna__top">
    <div class="container">
      <div class="row">
        <div class="col-lg-9 text-center m-auto">
          <div class="item">
            <a href="{{ url('/sostenibilidad') }}" class="botton botton--normal">Regresa</a>
            <h2>El Grupo Gloria adquirió la mayoría de Acciones de Agroindustrial Casa Grande. S.A.</h2>
            <span>27,junio 2018</span>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="novedades__interna__bottom">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 m-auto p-0">
          <div class="parrafoCentrado">
            <img src="{{ url('images/iconos/raya-cuadrado.png') }}" class="cuadrado" alt="">
            <p><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""> El 29 de enero de 2006 se cerró la Oferta Pública de Adquisición de acciones de Casa Grande habiendo obtenido el Grupo, a través de la empresa Corporación Azucarera del Perú S.A.A., el 57% de las acciones logrando de esta manera la mayoría accionaria. </p>
          </div>
        </div>
        <div class="col-lg-10 m-auto">
          <div class="owl-carousel owl-interna">
            <div class="item">
              <img src="{{ url('images/sostenibilidad/sostenibilidad/1.jpg') }}" alt="" class="img-fluid">
            </div>
            <div class="item">
              <img src="{{ url('images/sostenibilidad/sostenibilidad/1.jpg') }}" alt="" class="img-fluid">
            </div>
            <div class="item">
              <img src="{{ url('images/sostenibilidad/sostenibilidad/1.jpg') }}" alt="" class="img-fluid">
            </div>
          </div>
        </div>
        <div class="col-lg-6 m-auto p-0">
          <div class="contenido">
            <p>El Grupo tiene previsto invertir en Casa Grande US $ 60 millones en los próximos cinco años, los cuales se utilizarían exclusivamente para mejorar las condiciones industriales de la empresa y sembrar los campos que están disponibles.</p>
            <p>Casa Grande fue, en el siglo pasado, la primera y más grande industria azucarera del Perú y está ubica en el departamento de La Libertad. Cuenta con una extensión de más de 30,000 hectáreas de terreno para cultivo, de las cuales sólo un tercio se encuentra en producción.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>



@include('frontend.partials.footer')
@include('frontend.partials.modal')
@endsection

@section('scripts')

@stop
