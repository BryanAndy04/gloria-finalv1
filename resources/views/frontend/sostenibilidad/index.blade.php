@extends('frontend.layout.layout')
@section('content')
@include('frontend.partials.menu')
<span id='pestana_vista' valor='sosteniblidad'></span>

<section class="banner" style="background-image:url({{ url('images/banner/banner-sostenibilidad.jpg') }})">
    <div class="banner__titulo">
        <div>
            <h1>Sostenibilidad</h1>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. </p>
        </div>
    </div>
    <a class="scroll-down hash-link" href="#intro" title="Gloria">
  		<i>
  			<svg class="arrow" width="7" height="5" viewBox="0 0 7 5" xmlns="http://www.w3.org/2000/svg"><g fill="none" fill-rule="evenodd"><path id="arrow" fill="#FFF" fill-rule="nonzero" d="M0 0l3.5 5L7 0z"></path></g></svg>
  			<svg class="circle" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
          <g class="circle-wrap" fill="none" stroke="#fff" stroke-width="1" stroke-linejoin="round" stroke-miterlimit="10">
              <circle cx="12" cy="12" r="10.5"></circle>
          </g>
        </svg>
  		</i>
  	</a>
</section>
<section class="sostenibilidad seccionPage">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 m-auto">
        <div class="parrafoCentrado">
          <img src="{{ url('images/iconos/raya-cuadrado.png') }}" class="cuadrado" alt="">
          <p><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""> Morbi interdum mollis sapien. Sed ac risus. Phasellus lacinia, magna a ullamcorper laoreet, lectus arcu pulvinar risus, vitae facilisis libero dolor a purus. Sed vel lacus. Mauris nibh felis, adipiscing varius, adipiscing in, lacinia vel, tellus. </p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="secciones" style="padding-top:0px;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-7 p-0">
              <div class="owl-carousel owl-sostenibilidad">
                <div class="item">
                    <img src="{{ url('images/sostenibilidad/sostenibilidad/1.jpg') }}" class="img-fluid" alt="">
                </div>
                <div class="item">
                    <img src="{{ url('images/sostenibilidad/sostenibilidad/1.jpg') }}" class="img-fluid" alt="">
                </div>
             </div>
            </div>
            <div class="col-lg-5 d-flex align-items-center">
              <div class="secciones__content">
                  <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>Sostenibilidad </span></h2>
                  <div class="">
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</p>
                      <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. Phasellus pede arcu, dapibus eu, fermentum et, dapibus sed, urna.</p>
                  </div>
                  <a href="{{ url('sostenibilidad/interna') }}" class="botton botton--normal">Ver más</a>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7 p-0">
              <div class="owl-carousel owl-sostenibilidad">
                <div class="item">
                    <img src="{{ url('images/sostenibilidad/medioambiente/1.jpg') }}" class="img-fluid" alt="">
                </div>
                <div class="item">
                    <img src="{{ url('images/sostenibilidad/medioambiente/1.jpg') }}" class="img-fluid" alt="">
                </div>
              </div>
            </div>
            <div class="col-lg-5 d-flex align-items-center">
                <div class="secciones__content">
                    <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>Cuidado medio ambiente </span></h2>
                    <div class="">
                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</p>
                        <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. Phasellus pede arcu, dapibus eu, fermentum et, dapibus sed, urna.</p>
                    </div>
                    <a href="{{ url('sostenibilidad/interna') }}" class="botton botton--normal">Ver más</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7 p-0">
              <div class="owl-carousel owl-sostenibilidad">
                <div class="item">
                    <img src="{{ url('images/sostenibilidad/social/1.jpg') }}" class="img-fluid" alt="">
                </div>
                <div class="item">
                    <img src="{{ url('images/sostenibilidad/social/1.jpg') }}" class="img-fluid" alt="">
                </div>
              </div>
            </div>
            <div class="col-lg-5 d-flex align-items-center">
              <div class="secciones__content">
                  <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" alt=""><span>Responsabilidad social</span></h2>
                  <div class="">
                      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</p>
                      <p>Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing, commodo quis, gravida id, est. Sed lectus. Praesent elementum hendrerit tortor. Sed semper lorem at felis. Vestibulum volutpat, lacus a ultrices sagittis, mi neque euismod dui, eu pulvinar nunc sapien ornare nisl. Phasellus pede arcu, dapibus eu, fermentum et, dapibus sed, urna.</p>
                  </div>
                  <a href="{{ url('sostenibilidad/interna') }}" class="botton botton--normal">Ver más</a>
              </div>
            </div>
        </div>
    </div>
</section>
<section class="objetivos">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 offset-lg-1">
        <h2 class="titulo titulo--mediano"><img src="{{ url('images/iconos/cruz-rojo.png') }}" class="img-fluid" alt=""><span>Objetivos de</span><br> Desarrollo sostenible</h2>
        <p class="mb-5">El 25 de septiembre de 2015, los líderes mundiales adoptaron un conjunto de objetivos globales para erradicar la pobreza, proteger el planeta y asegurar la prosperidad para todos como parte de una nueva agenda de desarrollo sostenible. Cada objetivo tiene metas específicas que deben alcanzarse en los próximos 15 años.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-4">
        <div class="objetivos__item">
          <img src="{{ url('images/objetivos/1.jpg') }}" class="img-fluid" alt="">
          <div class="objetivos__item__texto">
            <h4>Fin de la pobreza</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4">
        <div class="objetivos__item">
          <img src="{{ url('images/objetivos/2.jpg') }}" class="img-fluid" alt="">
          <div class="objetivos__item__texto">
            <h4>Fin de la pobreza</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4">
        <div class="objetivos__item">
          <img src="{{ url('images/objetivos/3.jpg') }}" class="img-fluid" alt="">
          <div class="objetivos__item__texto">
            <h4>Fin de la pobreza</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4">
        <div class="objetivos__item">
          <img src="{{ url('images/objetivos/4.jpg') }}" class="img-fluid" alt="">
          <div class="objetivos__item__texto">
            <h4>Fin de la pobreza</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4">
        <div class="objetivos__item">
          <img src="{{ url('images/objetivos/5.jpg') }}" class="img-fluid" alt="">
          <div class="objetivos__item__texto">
            <h4>Fin de la pobreza</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4">
        <div class="objetivos__item">
          <img src="{{ url('images/objetivos/6.jpg') }}" class="img-fluid" alt="">
          <div class="objetivos__item__texto">
            <h4>Fin de la pobreza</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4">
        <div class="objetivos__item">
          <img src="{{ url('images/objetivos/7.jpg') }}" class="img-fluid" alt="">
          <div class="objetivos__item__texto">
            <h4>Fin de la pobreza</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4">
        <div class="objetivos__item">
          <img src="{{ url('images/objetivos/8.jpg') }}" class="img-fluid" alt="">
          <div class="objetivos__item__texto">
            <h4>Fin de la pobreza</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4">
        <div class="objetivos__item">
          <img src="{{ url('images/objetivos/9.jpg') }}" class="img-fluid" alt="">
          <div class="objetivos__item__texto">
            <h4>Fin de la pobreza</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4">
        <div class="objetivos__item">
          <img src="{{ url('images/objetivos/10.jpg') }}" class="img-fluid" alt="">
          <div class="objetivos__item__texto">
            <h4>Fin de la pobreza</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4">
        <div class="objetivos__item">
          <img src="{{ url('images/objetivos/11.jpg') }}" class="img-fluid" alt="">
          <div class="objetivos__item__texto">
            <h4>Fin de la pobreza</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4">
        <div class="objetivos__item">
          <img src="{{ url('images/objetivos/12.jpg') }}" class="img-fluid" alt="">
          <div class="objetivos__item__texto">
            <h4>Fin de la pobreza</h4>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@include('frontend.partials.footer')
@include('frontend.partials.modal')
@endsection

@section('scripts')

@stop
