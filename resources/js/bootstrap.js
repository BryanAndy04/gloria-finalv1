window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');
    var jQueryBridget = require('jquery-bridget');
    window.AOS = require('AOS');
    AOS.init({disable: 'mobile'});
    window.owlcarousel = require('owl.carousel');
    require('@fortawesome/fontawesome-free');
    window.Popper = require('popper.js').default;
    require('bootstrap');
    window.Masonry = require('masonry-layout');
    window.ImagesLoaded = require('imagesloaded');
    window.slick = require('slick-carousel');
    require('@fancyapps/fancybox');
    require('isotope-layout');

    jQueryBridget( 'owlCarousel', owlcarousel, $ );
    jQueryBridget( 'masonry', Masonry, $ );
    jQueryBridget( 'imagesLoaded', ImagesLoaded, $ );
    jQueryBridget( 'slick', slick, $);

} catch (e) {}
