//slider

//rutas en español

var ruta_es = new Array();

ruta_es['home'] = 'inicio';
ruta_es['group'] = 'grupo';
ruta_es['group/innovation'] = 'grupo/innovacion';
ruta_es['group/sustainable-development'] = 'grupo/desarrollo-sostenible';
ruta_es['group/social-responsability'] = 'grupo/responsabilidad-social';


//rutas en ingles

var ruta_en = new Array();

ruta_en['inicio'] = 'home';
ruta_en['grupo'] = 'group';
ruta_en['grupo/innovacion'] = 'group/innovation';
ruta_en['grupo/desarrollo-sostenible'] = 'group/sustainable-development';
ruta_en['grupo/responsabilidad-social'] = 'group/social-responsability';

$('a[idioma]').click(function () {
    $('.idiomas').find('a').removeClass('active');
    $(this).addClass('active');
    if ( $(this).attr('active') == 0 ) {

        $('.idiomas').find('a').attr('active', 0);
        $(this).attr('active',1);
        $(this).addClass('active');
        var idioma = $(this).attr('idioma');
        var URLorigin = $(location).attr('origin');
        var URLpathname = $(location).attr('pathname').replace('/', '');

        var ruta_;

        if (idioma == 'en') {
            ruta_ = URLorigin+'/'+ruta_en[URLpathname];
        } else if (idioma == 'es') {
            ruta_ = URLorigin+'/'+ruta_es[URLpathname];
        }

        window.location.href = ruta_;
    }

})




var total_owl_novedades = 0;



$('.owl-slider').owlCarousel({
    loop: true,
    animateOut: 'fadeOut',
    nav: false,
    singleItem: true,
    responsiveClass: true,
    autoplay: true,
    items: 1
})


//grupo historia

$('.historia-content').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    adaptiveHeight: true,
    asNavFor: '.historia-nav'
});

$('.historia-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.historia-content',
    dots: false,
    arrows: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [
        {
            breakpoint: 800,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

//tabs

$('#myTab a').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
})


//scroll-banner
$("#mouse-scroll").click(function () {
    // alert("hola");
    var x = $('.descripcion').offset().top + -90;
    $('html, body').animate({
        scrollTop: x
    }, 1500);
});

var $owl_novedades = $('.owl-novedades')

$owl_novedades.owlCarousel({
    margin: 30,
    nav: true,
    touchDrag: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            autoWidth: true,
            margin: 30,
            items: 3
        }
    },
    onInitialized: function (e) {
        total_owl_novedades = e.item.count;
    },
    onChange: function (e) {
        console.log(e);
    }
});


/*
$owl_novedades.on('translated.owl.carousel', function(event) {
  console.log(event);
})*/


// var $filtro= $('.novedades__filtro');
// var $filtroTop= $filtro.offset().top;
//
// var pegarFiltro = function(){
//   var $scrollTop = $(window).scrollTop();
//   if($scrollTop >= $filtroTop){
//     $filtro.addClass('stiky');
//   }
//   else{
//     $filtro.removeClass('stiky');
//   }
// }


// $(window).on('scroll',pegarFiltro);




$('.owl-ultimasNov, .owl-sostenibilidad').owlCarousel({
    loop: true,
    nav: false,
    items: 1,
    autoHeight: true
})


$('.owl-objetivos').owlCarousel({
    autoplay: true,
    margin: 60,
    nav: true,
    responsive: {
        0: {
            items: 2
        },
        600: {
            items: 3
        },
        1000: {
            items: 5
        },
        1200: {
            items: 8
        }
    }
});


if ($(window).width() <= 768) {
    $(".listados").click(function () {
        $('.mapa__content').css('transform', 'translateX(-700px)');
    });

}

$(".flechaMapa").click(function () {
    $('.mapa__content').css('transform', 'translateX(0)');
});





$(".bannerTop").click(function () {
    var x = $('.intro').offset().top + -80;
    $('html, body').animate({
        scrollTop: x
    }, 1500);
});

$(".hash-link").click(function () {
    var x = $('.seccionPage').offset().top + -85;
    $('html, body').animate({
        scrollTop: x
    }, 1500);
});

$(".bannerEmpresa").click(function () {
    var x = $('.empresaBotom').offset().top + -80;
    $('html, body').animate({
        scrollTop: x
    }, 1500);
});

$('.owl-conoce').owlCarousel({
    autoplay: false,
    nav: false,
    dots: false,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        1000: {
            items: 3
        }
    }
});


$('.owl-interna').owlCarousel({
    autoplay: true,
    items: 1
});



//Active Menu
var pestana = $('#pestana_vista').attr('valor');
$('#' + pestana).addClass('activo');



//conoce

$(".conoce__card").mouseover(function () {
    $(".conoce__cambioImagen").css("background-image", "url(" + $(this).attr('urlImages') + ")")
});


//select

$('select').each(function () {
    var $this = $(this), numberOfOptions = $(this).children('option').length;
    $this.addClass('select-hidden');
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function (e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function () {
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function (e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });

    $(document).click(function () {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});


//para el menu despegable
{
    setTimeout(() => document.body.classList.add('render'), 60);
    const navdemos = Array.from(document.querySelectorAll('nav.demos > .demo'));
    const total = navdemos.length;
    const current = navdemos.findIndex(el => el.classList.contains('demo--current'));
    const navigate = (linkEl) => {
        document.body.classList.remove('render');
        document.body.addEventListener('transitionend', () => window.location = linkEl.href);
    };
    navdemos.forEach(link => link.addEventListener('click', (ev) => {
        ev.preventDefault();
        navigate(ev.target);
    }));
    document.addEventListener('keydown', (ev) => {
        const keyCode = ev.keyCode || ev.which;
        let linkEl;
        if (keyCode === 37) {
            linkEl = current > 0 ? navdemos[current - 1] : navdemos[total - 1];
        }
        else if (keyCode === 39) {
            linkEl = current < total - 1 ? navdemos[current + 1] : navdemos[0];
        }
        else {
            return false;
        }
        navigate(linkEl);
    });
}


//
// these easing functions are based on the code of glsl-easing module.
// https://github.com/glslify/glsl-easings
//

const ease = {
    exponentialIn: (t) => {
        return t == 0.0 ? t : Math.pow(2.0, 10.0 * (t - 1.0));
    },
    exponentialOut: (t) => {
        return t == 1.0 ? t : 1.0 - Math.pow(2.0, -10.0 * t);
    },
    exponentialInOut: (t) => {
        return t == 0.0 || t == 1.0
            ? t
            : t < 0.5
                ? +0.5 * Math.pow(2.0, (20.0 * t) - 10.0)
                : -0.5 * Math.pow(2.0, 10.0 - (t * 20.0)) + 1.0;
    },
    sineOut: (t) => {
        const HALF_PI = 1.5707963267948966;
        return Math.sin(t * HALF_PI);
    },
    circularInOut: (t) => {
        return t < 0.5
            ? 0.5 * (1.0 - Math.sqrt(1.0 - 4.0 * t * t))
            : 0.5 * (Math.sqrt((3.0 - 2.0 * t) * (2.0 * t - 1.0)) + 1.0);
    },
    cubicIn: (t) => {
        return t * t * t;
    },
    cubicOut: (t) => {
        const f = t - 1.0;
        return f * f * f + 1.0;
    },
    cubicInOut: (t) => {
        return t < 0.5
            ? 4.0 * t * t * t
            : 0.5 * Math.pow(2.0 * t - 2.0, 3.0) + 1.0;
    },
    quadraticOut: (t) => {
        return -t * (t - 2.0);
    },
    quarticOut: (t) => {
        return Math.pow(t - 1.0, 3.0) * (1.0 - t) + 1.0;
    },
}

class ShapeOverlays {
    constructor(elm) {
        this.elm = elm;
        this.path = elm.querySelectorAll('path');
        this.numPoints = 2;
        this.duration = 600;
        this.delayPointsArray = [];
        this.delayPointsMax = 0;
        this.delayPerPath = 200;
        this.timeStart = Date.now();
        this.isOpened = false;
        this.isAnimating = false;
    }
    toggle() {
        this.isAnimating = true;
        for (var i = 0; i < this.numPoints; i++) {
            this.delayPointsArray[i] = 0;
        }
        if (this.isOpened === false) {
            this.open();
        } else {
            this.close();
        }
    }
    open() {
        this.isOpened = true;
        this.elm.classList.add('is-opened');
        this.timeStart = Date.now();
        this.renderLoop();
    }
    close() {
        this.isOpened = false;
        this.elm.classList.remove('is-opened');
        this.timeStart = Date.now();
        this.renderLoop();
    }
    updatePath(time) {
        const points = [];
        for (var i = 0; i < this.numPoints; i++) {
            const thisEase = this.isOpened ?
                (i == 1) ? ease.cubicOut : ease.cubicInOut :
                (i == 1) ? ease.cubicInOut : ease.cubicOut;
            points[i] = thisEase(Math.min(Math.max(time - this.delayPointsArray[i], 0) / this.duration, 1)) * 100
        }

        let str = '';
        str += (this.isOpened) ? `M 0 0 V ${points[0]} ` : `M 0 ${points[0]} `;
        for (var i = 0; i < this.numPoints - 1; i++) {
            const p = (i + 1) / (this.numPoints - 1) * 100;
            const cp = p - (1 / (this.numPoints - 1) * 100) / 2;
            str += `C ${cp} ${points[i]} ${cp} ${points[i + 1]} ${p} ${points[i + 1]} `;
        }
        str += (this.isOpened) ? `V 0 H 0` : `V 100 H 0`;
        return str;
    }
    render() {
        if (this.isOpened) {
            for (var i = 0; i < this.path.length; i++) {
                this.path[i].setAttribute('d', this.updatePath(Date.now() - (this.timeStart + this.delayPerPath * i)));
            }
        } else {
            for (var i = 0; i < this.path.length; i++) {
                this.path[i].setAttribute('d', this.updatePath(Date.now() - (this.timeStart + this.delayPerPath * (this.path.length - i - 1))));
            }
        }
    }
    renderLoop() {
        this.render();
        if (Date.now() - this.timeStart < this.duration + this.delayPerPath * (this.path.length - 1) + this.delayPointsMax) {
            requestAnimationFrame(() => {
                this.renderLoop();
            });
        }
        else {
            this.isAnimating = false;
        }
    }
}

(function () {
    const elmHamburger = document.querySelector('.hamburger');
    const gNavItems = document.querySelectorAll('.global-menu__item');
    const elmOverlay = document.querySelector('.shape-overlays');
    const overlay = new ShapeOverlays(elmOverlay);

    elmHamburger.addEventListener('click', () => {
        if (overlay.isAnimating) {
            return false;
        }
        overlay.toggle();
        if (overlay.isOpened === true) {
            elmHamburger.classList.add('is-opened-navi');
            for (var i = 0; i < gNavItems.length; i++) {
                gNavItems[i].classList.add('is-opened');
            }
        } else {
            elmHamburger.classList.remove('is-opened-navi');
            for (var i = 0; i < gNavItems.length; i++) {
                gNavItems[i].classList.remove('is-opened');
            }
        }
    });
}());
