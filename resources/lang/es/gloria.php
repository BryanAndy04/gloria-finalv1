<?php
return [

    //ruta

    'r_inicio' => 'inicio',
    'r_grupo' => 'grupo',
    'r_grupo_innovacion' => 'grupo/innovacion',
    'r_grupo_desarrollo_sostenible' => 'grupo/desarrollo-sostenible',
    'r_grupo_responsabilidad_social' => 'grupo/responsabilidad-social',

    //menu

    'inicio' => 'inicio',
    'elgrupo' => 'el grupo',
    'empresas' => 'empresas',
    'novedades' => 'novedades',
    'sosteniblidad' => 'sosteniblidad',
    'contactanos' => 'contáctanos',


    //page inicio

    'conoce' => 'conoce',
    'empresa' => 'nuestras empresas',
    'objetivos' => 'Objetivos de',
    'desarrollosostenible' => 'Desarrollo sostenible',
    'crecimiento' => 'crecimiento y fortalecimiento estratégico',
    'donde' => 'Dónde',
    'estamos' => 'estamos',
    'negocios' => 'Negocios presentes en Perú, como también en Bolivia, Colombia, Ecuador, Argentina y Puerto Rico',
    'novedades' => 'Novedades',


     // footer

     'siguenos' => 'Siguenos',
     'enlaces' => 'Enlaces',
     'r_proveedores' => 'SUPPLIER REGISTRATION',
     'trabajeconnosotros' => 'WORK WITH US',
     'privacidad' => 'PRIVACY POLICIES',
     'derechos' => 'Todos los derechos reservados',


     //page quienes_somos

    'quienes' => 'Quienes',
    'somos' => 'somos',
    'historia' => 'Historia',
    'inovacion' => 'Innovación',
    'desarrollo' => 'Desarrollo',
    'sostenible' => 'sostenible',
    'responsabilidad' => 'Responsabilidad',
    'social' => 'Social',


    //btn
    'vermas' => 'Ver más',
    'btn_contactenos' => 'Contáctenos',
    'btn_ver_novedades' => 'Ver novedades',
    'regresa' => 'Regresa',



];
