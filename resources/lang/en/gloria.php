<?php
return [


    //ruta

    'r_inicio' => 'home',
    'r_grupo' => 'group',
    'r_grupo_innovacion' => 'group/innovation',
    'r_grupo_desarrollo_sostenible' => 'group/sustainable-development',
    'r_grupo_responsabilidad_social' => 'group/social-responsability',

    //menu

    'inicio' => 'Home',
    'elgrupo' => 'The group',
    'empresas' => 'Business',
    'novedades' => 'novelties',
    'sosteniblidad' => 'sustainability',
    'contactanos' => 'contact us',




    // footer

    'siguenos' => 'Follow us',
    'enlaces' => 'Links',
    'r_proveedores' => 'SUPPLIER REGISTRATION',
    'trabajeconnosotros' => 'WORK WITH US',
    'privacidad' => 'PRIVACY POLICIES',


    //page inicio

    'conoce' => 'know',
    'empresa' => 'our companies',
    'objetivos' => 'Sustainable ',
    'desarrollosostenible' => 'Development Goals',
    'crecimiento' => 'growth and strategic strengthening',
    'donde' => 'Where',
    'estamos' => 'we are',
    'negocios' => 'Businesses present in Peru, as well as in Bolivia, Colombia, Ecuador, Argentina and Puerto Rico',
    'novedades' => 'New arrivals',


    //page quienes_somos

    'quienes' => 'About',
    'somos' => 'us',
    'historia' => 'History',
    'inovacion' => 'Innovation',
    'desarrollo' => 'Sustainable',
    'sostenible' => 'development',
    'responsabilidad' => 'Social',
    'social' => 'Responsability',





    //btn
    'vermas' => 'See more',
    'btn_contactenos' => 'Contact Us',
    'btn_ver_novedades' => "See what's new",
    'regresa' => 'Came back',


];
